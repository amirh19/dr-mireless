export interface OtherField {
  id?: Number;
  type: Number;
  cancer?: String;
  diabetes?: String;
  hypertension?: String;
  other?: String;
}

export default interface IClinicalHistory {
  id?: Number;
  other_fields: OtherField[];
  date: String; //"2020-06-28T11:45:19.558334-05:00",
  medicines: String;
  surgeries: String;
  smoking: String;
  alcoholic: String;
  drugs: String;
  exercise: String;
  user: Number;
  height: Number;
  weight: Number;
  blood_pressure: Number;
  body_temperature: Number;
  heart_rate: Number;
  breathing_frequency: Number;
}
