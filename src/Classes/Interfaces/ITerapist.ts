import ICommonData from "./ICommonData";
interface ITerapist extends ICommonData {
  gender: Number;
  birthday: String;
  specialty: String;
  professional_license: String;
  specialty_license: String;
}
export default ITerapist;
