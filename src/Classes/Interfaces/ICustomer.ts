interface Customer {
  id: Number;
  first_name: String;
  last_name: String;
  email: String;
  phone: String;
  gender: Number;
  birthday: String;
}
export default Customer;
