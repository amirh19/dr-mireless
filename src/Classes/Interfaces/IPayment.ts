export default interface IPayment {
  id?: Number;
  user: Number | String;
  date: String;
  paymentType: Number;
  billed: Boolean;
  quantity: Number;
  service: Number;
  branchOffice: Number | String;
  concept: String;
}
