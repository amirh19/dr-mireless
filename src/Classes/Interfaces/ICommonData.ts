interface CommonData {
  id: Number;
  first_name: String;
  last_name: String;
  phone: String;
  email: String;
}
export default CommonData;
