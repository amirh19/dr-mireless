export default interface INotification {
  id?: Number;
  users?: String;
  title: String;
  body: String;
  extra?: Number;
  notitication_type: Number;
  created_at?: String;
  read?: Boolean;
}
