import IAudio from "./IAudio";
interface IDisease {
  id: Number;
  name: String;
  audios: Array<IAudio>;
}
export default IDisease;
