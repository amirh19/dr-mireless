export default interface IAppointment {
  id?: Number;
  patient: Number;
  therapist: Number;
  date: String;
  hours: String;
  status: Number;
  branchOffice?: Number;
}
