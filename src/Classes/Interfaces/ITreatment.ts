import IAudio from "./IAudio";
export default interface ITreatment {
  users: Number[];
  name: String;
  audio: Number[] | IAudio[];
  date?: String;
  days_of_treatment?: Number;
}
