import ILoginData from "./Interfaces/ILoginData";
import ICommonData from "./Interfaces/ICommonData";
import Service from "./Service";

class User {
  id: Number;
  first_name: String;
  last_name: String;
  phone: String;
  email: String;
  location_id?: Number;
  location_name?: String;
  constructor(common: ICommonData) {
    if (new.target === User) {
      throw new TypeError("Cannot construct User instances directly");
    }
    this.id = common.id;
    this.first_name = common.first_name;
    this.last_name = common.last_name;
    this.phone = common.phone;
    this.email = common.email;
  }
  public async setLocation() {
    try {
      const service: Service = new Service({});
      const LocationResponse = await service.get(
        `v1/api/assign-branch-office/?user=${this.id}`
      );
      if (LocationResponse.error) {
        return (this.location_id = undefined);
      } else {
        this.location_name = LocationResponse.data[0].branch_office.name;
        this.location_id = LocationResponse.data[0].branch_office.id;
      }
    } catch (error) {
      this.location_id = undefined;
    }
  }
  public async searchUsers(name: String = "", lastName: String = "") {
    const service = new Service({});
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    const response = await service.get(
      `v1/api/assign-branch-office/?branch_office=${this.location_id}&first_name=${name}&last_name=${lastName}&type=3`
    );
    return response.error ? false : response.data;
  }
  public getLocation = async (id: Number) => {
    const service = new Service({});
    const response = await service.get(`v1/api/branch-office/${id}/`);
    return response.error ? false : response;
  };
  public static async login(loginData: ILoginData) {
    const service = new Service({});
    const response = await service.authenticate(loginData);
    if (response.success) {
      return response.user;
    } else {
      return false;
    }
  }
}
export default User;
