import axios, { AxiosResponse, AxiosRequestConfig } from "axios";
import jwt from "jsonwebtoken";
class Service {
  private BASE_URL: String;
  public config: AxiosRequestConfig;
  constructor(configuration: AxiosRequestConfig) {
    this.BASE_URL = process.env.REACT_APP_API_URL!;
    this.config = configuration;
    const token = localStorage.getItem("access_token");
    if (token) {
      this.config.headers = {
        Authorization: `Bearer ${token}`,
      };
    }
  }

  public post = async (endpoint: string, data: any) => {
    try {
      let response: AxiosResponse = await axios.post(
        `${this.BASE_URL}${endpoint}`,
        data,
        this.config
      );
      if (response.data.errors) {
        throw new Error(response.data.message);
      }
      return response.data;
    } catch (error) {
      console.error(error);
      localStorage.setItem(
        "error",
        error.response && error.response.data && error.response.data.errors
          ? error.response.data.errors[0]
          : "Algo inesperado sucedió"
      );
      //TO-DO handle UI messages here
      return {
        error: true,
      };
    }
  };
  public get = async (endpoint: String) => {
    try {
      const response = await axios.get(
        `${this.BASE_URL}${endpoint}`,
        this.config
      );
      return response.data;
    } catch (error) {
      console.log(error, endpoint);
      localStorage.setItem(
        "error",
        error.response && error.response.data && error.response.data.errors
          ? error.response.data.errors[0]
          : "Algo inesperado sucedió"
      );
      return { error: true };
    }
  };
  public put = async (endpoint: String, data: any) => {
    try {
      const response = await axios.put(
        `${this.BASE_URL}${endpoint}`,
        data,
        this.config
      );
      return response.data;
    } catch (error) {
      console.log(error);
      localStorage.setItem(
        "error",
        error.response && error.response.data && error.response.data.errors
          ? error.response.data.errors[0]
          : "Algo inesperado sucedió"
      );
      return { error: true };
    }
  };
  public delete = async (endpoint: String): Promise<Boolean> => {
    try {
      await axios.delete(`${this.BASE_URL}${endpoint}`, this.config);
      return true;
    } catch (error) {
      console.log(error);
      localStorage.setItem(
        "error",
        error.response && error.response.data && error.response.data.errors
          ? error.response.data.errors[0]
          : "Algo inesperado sucedió"
      );
      return false;
    }
  };
  public authenticate = async (user: any) => {
    try {
      let response: AxiosResponse = await axios.post(
        `${this.BASE_URL}token/`,
        user,
        this.config
      );
      if (response.data.errors) {
        throw new Error(response.data.message);
      } else {
        localStorage.setItem("access_token", response.data.access);
        localStorage.setItem("refresh", response.data.refresh);
        const decoded: any = jwt.decode(response.data.access);
        const userResponse = await axios.get(
          `${this.BASE_URL}users/${decoded.user_id}`,
          {
            headers: {
              Authorization: `Bearer ${response.data.access}`,
            },
          }
        );
        if (userResponse.data.errors) {
          throw new Error("Can not get user");
        } else {
          return { success: true, user: userResponse.data };
        }
      }
    } catch (error) {
      console.error(error);
      //TO-DO handle UI messages here
      return {
        error: true,
      };
    }
  };
  tokenAuth = async () => {
    try {
      const token: string | null = localStorage.getItem("access_token");
      if (token) {
        const decoded: any = jwt.decode(token);
        const userResponse = await axios.get(
          `${this.BASE_URL}users/${decoded.user_id}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        return userResponse.data.errors ? { error: true } : userResponse.data;
      } else {
        return false;
      }
    } catch (error) {
      console.error(error);
      return { error: true };
    }
  };
}
export default Service;
