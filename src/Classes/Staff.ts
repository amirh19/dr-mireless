import User from "./User";
import IAudio from "./Interfaces/IAudio";
import Service from "./Service";
import IDisease from "./Interfaces/IDisease";
import ITreatment from "./Interfaces/ITreatment";
class Staff extends User {
  addAudio = async (audio: IAudio): Promise<IAudio | false> => {
    const service: Service = new Service({});
    const formData: FormData = new FormData();
    formData.append("audio", audio.audio as File);
    formData.append("name", audio.name as string);
    formData.append("duration", audio.duration.toString());
    formData.append("order", audio.order.toString());
    const response = await service.post("v1/api/audios/", formData);
    if (response.error) {
      return false;
    } else {
      return response;
    }
  };
  getAudios = async (
    page: Number = 1
  ): Promise<{ data: IAudio[]; pagination: any } | false> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/audios/?page=${page}`);
    if (response.error) {
      return false;
    } else {
      const pages = response.pagination.links;
      return {
        data: response.data,
        pagination: {
          prev: pages.prev
            ? pages.prev.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
          next: pages.next
            ? pages.next.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
          last: pages.last
            ? pages.last.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
          first: pages.first
            ? pages.first.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
        },
      };
    }
  };
  updateAudio = async (audio: IAudio): Promise<IAudio | false> => {
    const service: Service = new Service({});
    const formData: FormData = new FormData();
    if (typeof audio.audio === "object") {
      formData.append("audio", audio.audio as File);
    }
    formData.append("id", audio.id.toString());
    formData.append("name", audio.name as string);
    formData.append("order", audio.order.toString());
    formData.append("duration", audio.duration.toString());
    const response = await service.put(`v1/api/audios/${audio.id}/`, formData);
    if (response.error) {
      return false;
    } else {
      return response;
    }
  };
  deleteAudio = async (id: Number) => {
    const service: Service = new Service({});
    return await service.delete(`v1/api/audios/${id}/`);
  };

  getDiseases = async (page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get("v1/api/disease/?page=" + page);
    if (response.error) {
      return false;
    } else {
      const pages = response.pagination.links;
      return {
        data: response.data,
        pagination: {
          prev: pages.prev
            ? pages.prev.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          next: pages.next
            ? pages.next.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          last: pages.last
            ? pages.last.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          first: pages.first
            ? pages.first.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
        },
      };
    }
  };

  addDisease = async (
    disease: IDisease
  ): Promise<(IDisease & { audios: Array<Number> }) | false> => {
    const service: Service = new Service({});
    const response = await service.post("v1/api/disease/", disease);
    if (response.error) {
      return false;
    } else {
      await service.post("v1/api/assign-audio/", {
        disease: response.id,
        audio: disease.audios.map(({ id }) => id),
      });
      return response;
    }
  };
  updateDisease = async (
    disease: IDisease,
    id: Number
  ): Promise<(IDisease & { audios: Array<Number> }) | false> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/disease/${disease.id}/`,
      disease
    );
    if (response.error) {
      return false;
    } else {
      await service.put(`v1/api/assign-audio/${id}/`, {
        disease: disease.id,
        id: id,
        audio: disease.audios.map(({ id }) => id),
      });
      return response;
    }
  };
  deleteDisease = async (id: Number): Promise<Boolean> => {
    const service: Service = new Service({});
    return await service.delete(`v1/api/disease/${id}/`);
  };
  getDiseaseName = async (id: Number): Promise<String> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/disease/${id}`);
    return response ? response.name : "";
  };
  getDiseaseAudios = async (
    id: Number
  ): Promise<
    Array<{ id: Number; disease: Number; audio: IAudio[] }> | Boolean
  > => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/assign-audio/?disease=${id}`);
    if (response.error) {
      return false;
    } else {
      return response.data;
    }
  };
  /**
   * Treatments
   */

  createTreatment = async (treatment: ITreatment) => {
    const service: Service = new Service({});
    const response = await service.post("v1/api/create-treatment/", treatment);
    return response.error ? false : response;
  };
}
export default Staff;
