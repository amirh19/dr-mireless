import User from "./User";
import ITerapist from "./Interfaces/ITerapist";
import Service from "./Service";
import IAppointment from "./Interfaces/IAppointment";
import ICustomer from "./Interfaces/ICustomer";
import View from "@fullcalendar/core/View";
import IClinicalHistory from "./Interfaces/IClinicalHistory";
import IMedicalConsultation from "./Interfaces/IMedicalConsultation";

class Terapist extends User implements ITerapist {
  gender: Number;
  birthday: String;
  specialty: String;
  professional_license: String;
  specialty_license: String;
  constructor(user: ITerapist) {
    super(user);
    this.gender = user.gender;
    this.birthday = user.birthday;
    this.specialty = user.specialty;
    this.professional_license = user.professional_license;
    this.specialty_license = user.specialty_license;
  }

  getAppointments = async (view: View): Promise<IAppointment[] | Boolean> => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    const service: Service = new Service({});
    let query: String = `v1/api/appointment/?branchOffice=${this.location_id}`;
    switch (view.type) {
      case "dayGridMonth":
      case "timeGridWeek":
        query += `&start_range=${view.currentStart
          .toISOString()
          .substr(0, 10)}&end_range=${view.currentEnd
          .toISOString()
          .substr(0, 10)}`;
        break;
      case "timeGridDay":
        query += `&date=${view.currentStart.toISOString().substr(0, 10)}`;
        break;
    }
    query += `&therapist=${this.id}`;
    const response = await service.get(query);
    return response.error ? false : response.data;
  };

  getCustomer = async (id: Number): Promise<ICustomer | Boolean> => {
    const service: Service = new Service({});
    const customer = await service.get(`users/${id}`);
    return customer.error ? false : customer;
  };

  getAppointment = async (id: String) => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/appointment/${id}`);
    return response.error ? false : response;
  };

  /**
   * Medical history
   */

  createClinicalHistory = async (
    history: IClinicalHistory
  ): Promise<IClinicalHistory | Boolean> => {
    const service: Service = new Service({});
    const response = await service.post(`v1/api/medical-history/`, history);
    return response.error ? false : response;
  };

  getClinicalHistory = async (
    user: Number
  ): Promise<IClinicalHistory[] | Boolean> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/medical-history/?user=${user}`);
    return response.error ? false : response.data;
  };

  updateClinicalHistory = async (
    history: IClinicalHistory
  ): Promise<IClinicalHistory | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/medical-history/${history.id}/`,
      history
    );
    return response.error ? false : response;
  };

  /**
   * Medical consultation
   */

  createMedicalConsultation = async (
    medicalConsultation: FormData
  ): Promise<IMedicalConsultation | Boolean> => {
    const service: Service = new Service({});
    const response = await service.post(
      "v1/api/medical-consultation/",
      medicalConsultation
    );
    return response.error ? false : response;
  };

  getMedicalConsultations = async (
    page: Number = 1,
    start_range?: String,
    end_range?: String,
    user?: Number
  ) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/medical-consultation/?owner=${this.id}&user=${
        user || ""
      }&start_range=${start_range}&end_range=${end_range}&page=${page}`
    );
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.match(/page=([\d]+)/m)[1]
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.match(/page=([\d]+)/m)[1]
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.match(/page=([\d]+)/m)[1]
              : null,
          },
        };
  };
}
export default Terapist;
