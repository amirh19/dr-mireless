import User from "./User";
import ILocation from "./Interfaces/ILocation";
import Service from "./Service";
import ITerapist from "./Interfaces/ITerapist";
import ICommonData from "./Interfaces/ICommonData";
import IAudio from "./Interfaces/IAudio";
import ICustomer from "./Interfaces/ICustomer";
import jwt from "jsonwebtoken";
import IDisease from "./Interfaces/IDisease";
import IBillingData from "./Interfaces/IBillingData";
import IAppointment from "./Interfaces/IAppointment";
import View from "@fullcalendar/core/View";
import IClinicalHistory from "./Interfaces/IClinicalHistory";
import IPayment from "./Interfaces/IPayment";
import ITreatment from "./Interfaces/ITreatment";
import INotification from "./Interfaces/INotification";
import ITimeControl from "./Interfaces/ITimeControl";
class SuperAdmin extends User {
  _setLocation = (id: Number) => {
    this.location_id = id;
  };
  addLocation = async (location: ILocation) => {
    const service: Service = new Service({});
    const response: any = await service.post("v1/api/branch-office/", location);
    if (response.error) {
      return false;
    } else {
      return response;
    }
  };
  getLocations = async (page: Number = 1, all?: Boolean) => {
    const service: Service = new Service({});
    let response: any = await service.get(`v1/api/branch-office/?page=${page}`);
    if (all) {
      response = await service.get(
        `v1/api/branch-office/?per_page=${response.pagination.total_rows}`
      );
    }
    if (response.error) {
      return false;
    } else {
      return {
        data: response.data,
        pagination: {
          first: 1,
          last: response.pagination.links.last
            ? response.pagination.links.last.replace(
                process.env.REACT_APP_API_URL + "v1/api/branch-office/?page=",
                ""
              )
            : null,
          next: response.pagination.links.next
            ? response.pagination.links.next.replace(
                process.env.REACT_APP_API_URL + "v1/api/branch-office/?page=",
                ""
              )
            : null,
          prev: response.pagination.links.prev
            ? response.pagination.links.prev.replace(
                process.env.REACT_APP_API_URL + "v1/api/branch-office/?page=",
                ""
              )
            : null,
        },
      };
    }
  };
  updateLocation = async (location: ILocation) => {
    const service: Service = new Service({});
    const response: any = await service.put(
      `v1/api/branch-office/${location.id}/`,
      location
    );
    if (response.error) {
      return false;
    } else {
      return response;
    }
  };
  registerUser = async (
    type: 2 | 4 | 5,
    user: (ITerapist | ICommonData) & {
      password: String;
      password_confirm: String;
    },
    location_id: Number
  ) => {
    let worker: any = user;
    worker.type = type;
    const service: Service = new Service({});
    const response: any = await service.post("register/", worker);
    if (response.error) {
      return false;
    } else {
      const decoded: any = jwt.decode(response.access);
      const userResponse = await service.get(`users/${decoded.user_id}`);
      if (userResponse.error) {
        return false;
      } else {
        const assign_response = await service.post(
          "v1/api/create-assign-branch-office/",
          { user: decoded.user_id, branch_office: location_id }
        );
        if (assign_response.id) {
          return userResponse;
        } else {
          return false;
        }
      }
    }
  };
  getUsers = async (type: 2 | 4 | 5, pagination: Number = 1) => {
    const service: Service = new Service({});
    const response: any = await service.get(
      `user-type/?page=${pagination}&type=${type}`
    );
    if (response.error) {
      return false;
    } else {
      return {
        data: response.data,
        pagination: {
          first: 1,
          last: response.pagination.links.last
            .replace(process.env.REACT_APP_API_URL + "user-type/?page=", "")
            .replace("&type=" + type, ""),
          next: response.pagination.links.next
            ? response.pagination.links.next
                .replace(process.env.REACT_APP_API_URL + "user-type/?page=", "")
                .replace("&type=" + type, "")
            : null,
          prev: response.pagination.links.prev
            ? response.pagination.links.prev
                .replace(process.env.REACT_APP_API_URL + "user-type/?page=", "")
                .replace("&type=" + type, "")
            : null,
        },
      };
    }
  };
  updateUser = async (user: ITerapist | ICommonData, location: Number) => {
    const service: Service = new Service({});
    const response = await service.put(`users/${user.id}/`, user);
    const assigned = await service.get(
      `v1/api/assign-branch-office/?user=${user.id}`
    );
    await service.put(
      `v1/api/update-assign-branch-office/${assigned.data[0].id}/`,
      {
        user: user.id,
        branch_office: location,
      }
    );
    if (!response.error) {
      return response;
    } else {
      return false;
    }
  };
  addCustomer = async (
    customer: ICustomer & { password: String; password_confirm: String }
  ) => {
    const service: Service = new Service({});
    const response = await service.post("register/", { ...customer, type: 3 });
    if (response.error) {
      return false;
    } else {
      const decoded: any = jwt.decode(response.access);
      const userResponse = await service.get(`users/${decoded.user_id}`);
      if (userResponse.error) {
        return false;
      } else {
        const assign_response = await service.post(
          "v1/api/create-assign-branch-office/",
          { user: decoded.user_id, branch_office: this.location_id }
        );
        if (assign_response.id) {
          return userResponse;
        } else {
          return false;
        }
      }
    }
  };
  getCustomers = async (page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=${page}&type=3`
    );
    if (response.error) {
      return false;
    } else {
      return {
        data: response.data,
        pagination: {
          first: 1,
          last: response.pagination.links.last
            .replace(
              process.env.REACT_APP_API_URL +
                `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
              ""
            )
            .replace("&type=3", ""),
          next: response.pagination.links.next
            ? response.pagination.links.next
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=3", "")
            : null,
          prev: response.pagination.links.prev
            ? response.pagination.links.prev
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=3", "")
            : null,
        },
      };
    }
  };
  getCustomer = async (id: Number): Promise<ICustomer | Boolean> => {
    const service: Service = new Service({});
    const customer = await service.get(`users/${id}`);
    return customer.error ? false : customer;
  };
  updateCustomer = async (
    customer: ICustomer
  ): Promise<ICustomer | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(`users/${customer.id}/`, customer);
    return response.error ? false : response;
  };
  createBilling = async (
    id: Number,
    billingData: IBillingData
  ): Promise<IBillingData | Boolean> => {
    const service: Service = new Service({});
    const billingDataResponse = await service.post("v1/api/create-billing/", {
      user: id,
      ...billingData,
    });
    return billingDataResponse.error ? false : billingDataResponse;
  };
  getBillingByUser = async (id: Number): Promise<IBillingData | Boolean> => {
    const service: Service = new Service({});
    const billing = await service.get(`v1/api/billing/?user=${id}`);
    return billing.error
      ? false
      : billing.data.length > 0
      ? billing.data[0]
      : false;
  };
  updateBilling = async (
    billingData: IBillingData
  ): Promise<IBillingData | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/billing/${billingData.id}/`,
      billingData
    );
    return response.error ? false : response;
  };
  addAudio = async (audio: IAudio): Promise<IAudio | false> => {
    const service: Service = new Service({});
    const formData: FormData = new FormData();
    formData.append("audio", audio.audio as File);
    formData.append("name", audio.name as string);
    formData.append("duration", audio.duration.toString());
    formData.append("order", audio.order.toString());
    const response = await service.post("v1/api/audios/", formData);
    if (response.error) {
      return false;
    } else {
      return response;
    }
  };
  getAudios = async (
    page: Number = 1
  ): Promise<{ data: IAudio[]; pagination: any } | false> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/audios/?page=${page}`);
    if (response.error) {
      return false;
    } else {
      const pages = response.pagination.links;
      return {
        data: response.data,
        pagination: {
          prev: pages.prev
            ? pages.prev.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
          next: pages.next
            ? pages.next.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
          last: pages.last
            ? pages.last.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
          first: pages.first
            ? pages.first.replace(
                process.env.REACT_APP_API_URL + "v1/api/audios/?page=",
                ""
              )
            : null,
        },
      };
    }
  };
  updateAudio = async (audio: IAudio): Promise<IAudio | false> => {
    const service: Service = new Service({});
    const formData: FormData = new FormData();
    if (typeof audio.audio === "object") {
      formData.append("audio", audio.audio as File);
    }
    formData.append("id", audio.id.toString());
    formData.append("name", audio.name as string);
    formData.append("order", audio.order.toString());
    formData.append("dur6ation", audio.duration.toString());
    const response = await service.put(`v1/api/audios/${audio.id}/`, formData);
    if (response.error) {
      return false;
    } else {
      return response;
    }
  };
  deleteAudio = async (id: Number) => {
    const service: Service = new Service({});
    return await service.delete(`v1/api/audios/${id}/`);
  };
  getDiseases = async (page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get("v1/api/disease/?page=" + page);
    if (response.error) {
      return false;
    } else {
      const pages = response.pagination.links;
      return {
        data: response.data,
        pagination: {
          prev: pages.prev
            ? pages.prev.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          next: pages.next
            ? pages.next.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          last: pages.last
            ? pages.last.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
          first: pages.first
            ? pages.first.replace(
                process.env.REACT_APP_API_URL + "v1/api/disease/?page=",
                ""
              )
            : null,
        },
      };
    }
  };
  addDisease = async (
    disease: IDisease
  ): Promise<(IDisease & { audios: Array<Number> }) | false> => {
    const service: Service = new Service({});
    const response = await service.post("v1/api/disease/", disease);
    if (response.error) {
      return false;
    } else {
      await service.post("v1/api/assign-audio/", {
        disease: response.id,
        audio: disease.audios.map(({ id }) => id),
      });
      return response;
    }
  };
  updateDisease = async (
    disease: IDisease,
    id: Number
  ): Promise<(IDisease & { audios: Array<Number> }) | false> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/disease/${disease.id}/`,
      disease
    );
    if (response.error) {
      return false;
    } else {
      await service.put(`v1/api/assign-audio/${id}/`, {
        disease: disease.id,
        id: id,
        audio: disease.audios.map(({ id }) => id),
      });
      return response;
    }
  };
  deleteDisease = async (id: Number): Promise<Boolean> => {
    const service: Service = new Service({});
    return await service.delete(`v1/api/disease/${id}/`);
  };

  getDiseaseName = async (id: Number): Promise<String> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/disease/${id}`);
    return response ? response.name : "";
  };

  getDiseaseAudios = async (
    id: Number
  ): Promise<
    Array<{ id: Number; disease: Number; audio: IAudio[] }> | Boolean
  > => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/assign-audio/?disease=${id}`);
    if (response.error) {
      return false;
    } else {
      return response.data;
    }
  };
  getTerapists = async (page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=${page}&type=4`
    );
    if (response.error) {
      return false;
    } else {
      return {
        data: response.data,
        pagination: {
          first: 1,
          last: response.pagination.links.last
            .replace(
              process.env.REACT_APP_API_URL +
                `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
              ""
            )
            .replace("&type=4", ""),
          next: response.pagination.links.next
            ? response.pagination.links.next
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=4", "")
            : null,
          prev: response.pagination.links.prev
            ? response.pagination.links.prev
                .replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/assign-branch-office/?branch_office=${this.location_id}&page=`,
                  ""
                )
                .replace("&type=4", "")
            : null,
        },
      };
    }
  };
  /**
   * Appointment
   */
  scheduleAppointment = async (appointment: IAppointment) => {
    appointment.branchOffice = this.location_id as Number;
    const service: Service = new Service({});
    const response = await service.post("v1/api/appointment/", appointment);
    return response.error ? false : response;
  };

  getAppointments = async (
    view: View,
    therapist?: Number,
    status?: Number
  ): Promise<IAppointment[] | Boolean> => {
    const service: Service = new Service({});
    let query: String = `v1/api/appointment/?branchOffice=${this.location_id}`;
    switch (view.type) {
      case "dayGridMonth":
      case "timeGridWeek":
        query += `&start_range=${view.currentStart
          .toISOString()
          .substr(0, 10)}&end_range=${view.currentEnd
          .toISOString()
          .substr(0, 10)}`;
        break;
      case "timeGridDay":
        query += `&date=${view.currentStart.toISOString().substr(0, 10)}`;
        break;
    }
    if (therapist !== undefined) {
      query += `&therapist=${therapist}`;
    }
    if (status !== undefined) {
      query += `&status=${status}`;
    }
    let response = await service.get(query);
    if (!response.error) {
      response = await service.get(
        query + "&per_page=" + response.pagination.total_rows
      );
    } else {
      return false;
    }
    return response.error ? false : response.data;
  };

  getAppointment = async (id: String) => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/appointment/${id}`);
    return response.error ? false : response;
  };

  updateAppointment = async (
    event: IAppointment
  ): Promise<IAppointment | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/appointment/${event.id}/`,
      event
    );
    return response.error ? false : response;
  };

  /**
   * Medical history
   */

  createClinicalHistory = async (
    history: IClinicalHistory
  ): Promise<IClinicalHistory | Boolean> => {
    const service: Service = new Service({});
    const response = await service.post(`v1/api/medical-history/`, history);
    return response.error ? false : response;
  };

  getClinicalHistory = async (
    user: Number
  ): Promise<IClinicalHistory[] | Boolean> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/medical-history/?user=${user}`);
    return response.error ? false : response.data;
  };

  updateClinicalHistory = async (
    history: IClinicalHistory
  ): Promise<IClinicalHistory | Boolean> => {
    const service: Service = new Service({});
    const response = await service.put(
      `v1/api/medical-history/${history.id}/`,
      history
    );
    return response.error ? false : response;
  };

  /**
   *
   * Payments
   */
  createPayment = async (payment: IPayment): Promise<Boolean> => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    const service = new Service({});
    payment.branchOffice = this.location_id as Number;
    const response = await service.post("v1/api/payment/", payment);
    return response.error ? false : true;
  };

  sendByEmail = async (formData: FormData): Promise<Boolean> => {
    const service: Service = new Service({});
    const response = await service.post(`v1/api/receipt/`, formData);
    return response.error ? false : true;
  };

  getPayments = async (
    page: Number,
    start_range?: String,
    end_range?: String,
    user?: Number
  ): Promise<{ data: IPayment[]; pagination: any } | Boolean> => {
    if (this.location_id === undefined) {
      await this.setLocation();
    }
    let query: String = `v1/api/payment/?branchOffice=${this.location_id}&page=${page}`;
    if (start_range) {
      query += `&start_range=${start_range}`;
    }
    if (end_range) {
      query += `&end_range=${end_range}`;
    }
    if (user != undefined) {
      query += `&user=${user}`;
    }
    const service: Service = new Service({});
    const response = await service.get(query);
    if (response.error) return false;
    response.data.payments = await Promise.all(
      response.data.payments.map(async (payment: IPayment) => {
        const user = await this.getCustomer(payment.user as Number);
        const branch = await this.getLocation(payment.branchOffice as Number);
        return {
          ...payment,
          user: `${(user as ICustomer).first_name} ${
            (user as ICustomer).last_name
          }`,
          branchOffice: branch.shortName,
        };
      })
    );
    return response.error
      ? false
      : {
          data: response.data.payments,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.match(/page=([\d]+)/m)[1]
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.match(/page=([\d]+)/m)[1]
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.match(/page=([\d]+)/m)[1]
              : null,
          },
        };
  };
  /**
   * Treatments
   */

  createTreatment = async (treatment: ITreatment) => {
    const service: Service = new Service({});
    const response = await service.post("v1/api/create-treatment/", treatment);
    return response.error ? false : response;
  };
  getTreatments = async (id: Number, page: Number = 1) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/treatment/?users=${id}&page=${page}`
    );
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/treatment/?users=${id}&page=`,
                  ""
                )
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/treatment/?users=${id}&page=`,
                  ""
                )
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.replace(
                  process.env.REACT_APP_API_URL +
                    `v1/api/treatment/?users=${id}&page=`,
                  ""
                )
              : null,
          },
        };
  };

  /**
   *  Notifications
   *
   */

  addNotification = async (notification: INotification) => {
    const service = new Service({});
    const response = await service.post(
      "v1/api/notification-register/",
      notification
    );
    return response.error ? false : response;
  };

  getAllNotifications = async (
    page: Number = 1
  ): Promise<
    | Boolean
    | {
        data: INotification[];
        pagination: { first: Number; last: Number; prev: Number; next: Number };
      }
  > => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/notification/?page=${page}`);
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.replace(
                  process.env.REACT_APP_API_URL + `v1/api/notification/?page=`,
                  ""
                )
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.replace(
                  process.env.REACT_APP_API_URL + `v1/api/notification/?page=`,
                  ""
                )
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.replace(
                  process.env.REACT_APP_API_URL + `v1/api/notification/?page=`,
                  ""
                )
              : null,
          },
        };
  };

  /**
   * TimeControl
   */

  getTimeControl = async (): Promise<ITimeControl | Boolean> => {
    const service: Service = new Service({});
    const response = await service.get(`v1/api/time-control/`);
    return response.error
      ? false
      : response.data.length > 0
      ? response.data[0]
      : false;
  };

  saveTimeControl = async (
    timeControl: ITimeControl
  ): Promise<ITimeControl | Boolean> => {
    const service = new Service({});
    const response = await service.put(
      `v1/api/time-control/${timeControl.id}/`,
      timeControl
    );
    return response.error ? false : response;
  };

  getMedicalConsultations = async (
    page: Number = 1,
    start_range?: String,
    end_range?: String,
    user?: Number,
    therapist?: Number
  ) => {
    const service: Service = new Service({});
    const response = await service.get(
      `v1/api/medical-consultation/?owner=${therapist || ""}&user=${
        user || ""
      }&start_range=${start_range}&end_range=${end_range}&page=${page}&branchOffice=${
        this.location_id
      }`
    );
    return response.error
      ? false
      : {
          data: response.data,
          pagination: {
            first: 1,
            last: response.pagination.links.last
              ? response.pagination.links.last.match(/page=([\d]+)/m)[1]
              : null,
            next: response.pagination.links.next
              ? response.pagination.links.next.match(/page=([\d]+)/m)[1]
              : null,
            prev: response.pagination.links.prev
              ? response.pagination.links.prev.match(/page=([\d]+)/m)[1]
              : null,
          },
        };
  };
}
export default SuperAdmin;
