import React, { Suspense } from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import UserContext from "./Context/User";
import ToastContext from "./Context/Toast";
import Staff from "./Classes/Staff";
import SuperAdmin from "./Classes/SuperAdmin";
import Toast, { AlertMessage } from "./Components/ActionModals/Toast";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import Loader from "./Components/Loader/Loader";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#0077c8",
      main: "#0077c8",
      dark: "#0077c8",
      contrastText: "#fff",
    },
    secondary: {
      light: "#00a9e0",
      main: "#00a9e0",
      dark: "#00a9e0",
      contrastText: "#fff",
    },
  },
});

const Login = React.lazy(() => import("./Views/Login"));
const Panel = React.lazy(() => import("./Views/Panel"));

class App extends React.Component<
  {},
  { user: Staff | SuperAdmin | String; alerts: AlertMessage[] }
> {
  state = {
    user: "",
    alerts: [],
  };

  setUser = async (user: Staff | SuperAdmin) => {
    this.setState({ user: user });
  };

  setAlert = (alert: AlertMessage) => {
    const { alerts } = this.state;
    alerts.push(alert as never);
    this.setState({ alerts: alerts }, () =>
      setTimeout(() => {
        const { alerts } = this.state;
        alerts.splice(0);
        this.setState({ alerts: alerts });
      }, 6000)
    );
  };

  render() {
    return (
      <Router>
        <Switch>
          <ThemeProvider theme={theme}>
            <UserContext.Provider
              value={{ user: this.state.user, setUser: this.setUser }}
            >
              <ToastContext.Provider
                value={{ alerts: this.state.alerts, setAlert: this.setAlert }}
              >
                {!this.state.user && (
                  <Route path="/">
                    <Suspense fallback={<Loader />}>
                      <Login />
                    </Suspense>
                  </Route>
                )}

                {this.state.user && (
                  <Route path="/">
                    <Suspense fallback={<Loader />}>
                      <Panel />
                    </Suspense>
                  </Route>
                )}
                <Toast
                  alerts={this.state.alerts}
                  handleClose={(index: number) => {
                    const alerts = this.state.alerts;
                    alerts.splice(index);
                    this.setState({ alerts: alerts });
                  }}
                />
              </ToastContext.Provider>
            </UserContext.Provider>
          </ThemeProvider>
        </Switch>
      </Router>
    );
  }
}

export default App;
