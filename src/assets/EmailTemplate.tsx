const EmailTemplate: String = `
  <!DOCTYPE html>
  <html lang="es">
    <head>
      <meta charset="utf-8">
      <title>Example 1</title>
      <style>
      html,body{
        width: 21cm;  
        height: 29.7cm;
        font-size:8px;
        letter-spacing:0px;
      }
      h1{font-size:16px;}
      h2{font-size:14px;}
      h3{font-size:13px;}
      h4{font-size:12px;}
      h5{font-size:11px;}
      h6{font-size:10px;}
      </style>
    </head>
    <body>
      <table>
        <thead>
          <tr>
            <th class="service">DR MIRELES BIOFRECUENCIAS</th>
          </tr>
        </thead>
      </table>
      <table>
        <thead>
          <tr>
            <th class="service">Costo</th>
            <th>Método de pago</th>
          </tr>
          <tr>
            <th class="service">$1,500.00</th>
            <th>Tarjeta</th>
          </tr>
        </thead>
      </table>
    </body>
  </html>
  `;
export default EmailTemplate;
