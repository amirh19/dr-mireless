import React from "react";
import UserContext from "../../Context/User";
import Add from "../../Components/ActionModals/Add";
import {
  Container,
  ButtonGroup,
  Button,
  Grid,
  Typography,
  Chip,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from "@material-ui/core";
import { withRouter, RouteComponentProps } from "react-router-dom";
import CreateAppointment from "../../Components/Form/Appointment";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import withAlert from "../../Components/HOC/withAlert";
import ITerapist from "../../Classes/Interfaces/ITerapist";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import IAppointment from "../../Classes/Interfaces/IAppointment";
import Calendar from "../../Components/Calendar/Calendar";
import { View, EventApi } from "@fullcalendar/core";
import Pagination from "../../Components/Buttons/Pagination";
import Filter from "../../Components/ActionModals/Filter";
import ExportXSLX from "../../Components/DataVisual/ExportXSLX";
import moment from "moment";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";

interface State extends IAppointment {
  open: boolean;
  edit: boolean;
  customerName: String;
  pagination: any;
  terapists: ITerapist[];
  events: IAppointment[];
  view: View | null;
  statusFilter: Number;
  start_range: Date;
  end_range: Date;
}
interface Props extends RouteComponentProps<{ id?: string }> {
  showAlert: (alert: AlertMessage) => void;
  showError: Function;
}
const today = moment();
today.set({ minute: 0, second: 0, millisecond: 0 });
const initialState: State = {
  id: 0,
  start_range: moment().startOf("month").toDate(),
  end_range: today.toDate(),
  date: today.format("YYYY-MM-DD"),
  hours: today.format("HH:mm"),
  patient: 0, //customer id
  customerName: "",
  therapist: -1,
  status: 0,
  branchOffice: 0,
  open: false,
  edit: false,
  terapists: [],
  events: [],
  pagination: {
    first: 0,
    last: 0,
    next: 0,
    prev: 0,
  },
  view: null,
  statusFilter: -1,
};

class Appointment extends React.Component<Props, State> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = initialState;

  async componentDidMount() {
    if (this.props.match.params.id) {
      let customer = "";
      const worker = this.context.user as Secretary | SuperAdmin;
      const response = await worker.getCustomer(
        parseInt(this.props.match.params.id)
      );
      if (response) {
        customer =
          (response as ICustomer).first_name +
          " " +
          (response as ICustomer).last_name;
      }
      this.setState({
        open: true,
        customerName: customer,
        patient: parseInt(this.props.match.params.id),
      });
    }
    await this.getTerapists(1);
  }

  handleModal = () => {
    if (this.state.open) {
      this.setState((state) => ({
        ...initialState,
        open: !state.open,
        terapists: state.terapists,
        pagination: state.pagination,
      }));
    } else {
      this.props.history.push("/pacientes");
    }
  };

  getTerapists = async (page: Number = 1) => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.getTerapists(page);
    if (response) {
      this.setState({
        terapists:
          response.data.length > 0
            ? response.data.map((relation: any) => relation.user)
            : [],
        pagination: response.pagination,
      });
    } else {
      this.props.showError();
    }
  };

  onViewChange = (viewInfo: { el: HTMLElement; view: View }) => {
    console.log("view changed");
    this.getEvents(viewInfo.view);
  };

  getEvents = async (view: View) => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.getAppointments(
      view,
      this.state.therapist !== -1 ? this.state.therapist : undefined,
      this.state.statusFilter !== -1 ? this.state.statusFilter : undefined
    );
    console.log(response);
    if (response) {
      this.setState({ view: view, events: response as IAppointment[] });
    } else {
      this.props.showError();
    }
  };

  addAppointment = async () => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.scheduleAppointment({
      patient: this.state.patient,
      therapist: this.state.therapist,
      date: this.state.date,
      hours: this.state.hours,
      status: 2,
    });
    if (response) {
      const { events, terapists } = this.state;
      events.push(response as IAppointment);
      this.setState(
        { ...initialState, terapists: terapists, events: events },
        () => {
          this.props.showAlert({
            message: "Se ha creado una cita",
            severity: "success",
          });
          this.props.history.push("/citas");
        }
      );
    } else {
      this.props.showError();
    }
  };

  getAppointment = async (id: String) => {
    const worker = this.context.user as SuperAdmin | Secretary;
    const response = await worker.getAppointment(id);
    let customer: String = "";
    if (response) {
      const custResponse = await worker.getCustomer(response.patient);
      if (custResponse) {
        customer =
          (custResponse as ICustomer).first_name +
          " " +
          (custResponse as ICustomer).last_name;
      }
      this.setState({
        ...response,
        customerName: customer,
        open: true,
        edit: true,
      });
    }
  };

  updateAppointment = async () => {
    const worker = this.context.user as SuperAdmin | Secretary;
    const response = await worker.updateAppointment({
      id: this.state.id,
      patient: this.state.patient,
      therapist: this.state.therapist,
      date: this.state.date,
      hours: this.state.hours,
      status: this.state.status,
    });
    if (response) {
      const { events } = this.state;
      const updated: IAppointment[] = events.map((event) =>
        event.id === (response as IAppointment).id
          ? (response as IAppointment)
          : event
      );
      this.setState(
        (state) => ({
          ...initialState,
          terapists: state.terapists,
          events: updated,
        }),
        () =>
          this.props.showAlert({
            message: "Se ha actualizado una cita",
            severity: "success",
          })
      );
    } else {
      this.props.showError();
    }
  };

  getStatus = (id: Number) => {
    let name: String;
    switch (id) {
      case 1:
        name = "Cancelado";
        break;
      case 2:
        name = "Pendiente";
        break;
      case 3:
        name = "Confirmado";
        break;
      case 4:
        name = "Atendido";
        break;
      case 5:
        name = "No atendido";
        break;
      default:
        name = "sin datos";
    }
    return name;
  };

  render() {
    return (
      <div>
        <Grid container spacing={2}>
          <Grid item sm={1} xs={6}>
            <Add handleModal={this.handleModal} open={this.state.open}>
              <>
                <form>
                  <Container maxWidth="md">
                    <CreateAppointment
                      edit={this.state.edit}
                      customer={this.state.customerName}
                      getPage={this.getTerapists}
                      patient={this.state.patient}
                      pagination={this.state.pagination}
                      terapists={this.state.terapists}
                      date={
                        (moment(
                          `${this.state.date} ${this.state.hours}:00`
                        ).local() as unknown) as Date
                      }
                      changeDate={(date: any) => {
                        //2011-10-05T14:48:00.000Z iso string
                        const _date: String = moment(date).format(
                          "YYYY-MM-DD HH:mm:ss"
                        );
                        const the_date = _date.substr(0, 10);
                        const time = _date.substr(11, 5);
                        this.setState({ date: the_date, hours: time });
                      }}
                      selected={this.state.therapist}
                      select={(id: Number) => this.setState({ therapist: id })}
                      status={this.state.status}
                      changeStatus={(
                        e: React.ChangeEvent<{ value: unknown }>
                      ) => {
                        this.setState({
                          status: parseInt(e.target.value as string),
                        });
                      }}
                      id={this.state.id}
                    />
                  </Container>
                </form>
                <ButtonGroup
                  style={{
                    marginTop: "2rem",
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    position: "absolute",
                    bottom: 10,
                  }}
                  size="large"
                  color="default"
                >
                  <Button
                    disabled={
                      this.state.patient === 0 || this.state.therapist === -1
                    }
                    onClick={
                      this.state.edit
                        ? this.updateAppointment
                        : this.addAppointment
                    }
                  >
                    {this.state.edit ? "Actualizar" : "Agregar"}
                  </Button>
                  <Button
                    onClick={() => this.setState({ open: false, edit: false })}
                  >
                    Cancelar
                  </Button>
                </ButtonGroup>
              </>
            </Add>
          </Grid>
          <Grid item sm={1} xs={6}>
            <Filter>
              <Grid item xs={12}>
                <ExpansionPanel>
                  <ExpansionPanelSummary>
                    <Typography
                      style={{
                        width: "100%",
                        textAlign: "center",
                        fontWeight: "bold",
                        color: "var(--principal)",
                      }}
                      variant="body1"
                    >
                      Filtrar por terapeuta
                    </Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails
                    style={{ display: "flex", flexDirection: "column" }}
                  >
                    <div
                      style={{
                        width: "100%",
                        maxHeight: "70%",
                        overflowY: "auto",
                      }}
                    >
                      <Chip
                        style={{
                          width: "100%",
                          paddingRight: "1rem",
                          paddingLeft: "1rem",
                          marginBottom: "1rem",
                          textAlign: "center",
                          backgroundColor:
                            this.state.therapist === -1
                              ? "var(--principal)"
                              : "white",
                          color:
                            this.state.therapist === -1 ? "white" : "black",
                        }}
                        label="Todos"
                        onClick={() => {
                          this.setState({ therapist: -1 }, () =>
                            this.getEvents(this.state.view as View)
                          );
                        }}
                      />
                      {this.state.terapists.map((terapist: ITerapist) => (
                        <Chip
                          style={{
                            width: "100%",
                            paddingRight: "1rem",
                            paddingLeft: "1rem",
                            marginBottom: "1rem",
                            textAlign: "center",
                            backgroundColor:
                              this.state.therapist === terapist.id
                                ? "var(--principal)"
                                : "white",
                            color:
                              this.state.therapist === terapist.id
                                ? "white"
                                : "black",
                          }}
                          label={`${terapist.first_name} ${terapist.last_name}`}
                          onClick={() =>
                            this.setState({ therapist: terapist.id }, () =>
                              this.getEvents(this.state.view as View)
                            )
                          }
                        />
                      ))}
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-around",
                      }}
                    >
                      <Pagination
                        pagination={this.state.pagination}
                        getData={this.getTerapists}
                      />
                    </div>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>
              <Grid item xs={12}>
                <ExpansionPanel>
                  <ExpansionPanelSummary>
                    <Typography
                      style={{
                        width: "100%",
                        textAlign: "center",
                        fontWeight: "bold",
                        color: "var(--principal)",
                      }}
                      variant="body1"
                    >
                      Filtrar por estatus
                    </Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails
                    style={{ display: "flex", flexDirection: "column" }}
                  >
                    <div
                      style={{
                        width: "100%",
                        maxHeight: "70%",
                        overflowY: "auto",
                      }}
                    >
                      {[
                        { name: "Todos", value: -1 },
                        { name: "Cancelado", value: 1 },
                        { name: "Pendiente", value: 2 },
                        { name: "Confirmado", value: 3 },
                        { name: "Atendido", value: 4 },
                        { name: "No atendido", value: 5 },
                      ].map((status: any) => (
                        <Chip
                          style={{
                            width: "100%",
                            paddingRight: "1rem",
                            paddingLeft: "1rem",
                            marginBottom: "1rem",
                            textAlign: "center",
                            backgroundColor:
                              status.value === this.state.statusFilter
                                ? "var(--principal)"
                                : "white",
                            color:
                              status.value === this.state.statusFilter
                                ? "white"
                                : "black",
                          }}
                          label={status.name}
                          onClick={() =>
                            this.setState({ statusFilter: status.value }, () =>
                              this.getEvents(this.state.view as View)
                            )
                          }
                        />
                      ))}
                    </div>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>
              <Grid container justify="flex-end" item xs={12}>
                <ExportXSLX
                  endpoint={`v1/api/appointment?therapist=${
                    this.state.therapist === -1 ? "" : this.state.therapist
                  }&status=${this.state.status === 0 ? "" : this.state.status}`}
                  title="Citas"
                  col_titles={[
                    "ID",
                    "PACIENTE",
                    "TERAPEUTA",
                    "FECHA",
                    "HORA",
                    "ESTADO",
                    "OFICINA",
                  ]}
                  withDateRange
                  onStartChange={(date: MaterialUiPickersDate) => {
                    this.setState({
                      start_range: moment(date).toDate(),
                    });
                  }}
                  onEndChange={(date: MaterialUiPickersDate) => {
                    this.setState({
                      end_range: moment(date).toDate(),
                    });
                  }}
                  start={this.state.start_range}
                  end={this.state.end_range}
                  extraProcess={async (appointments: IAppointment[]) => {
                    let data = [];
                    data = await Promise.all(
                      appointments.map(async (appointment: IAppointment) => {
                        const [user, therapist, branch] = await Promise.all([
                          (this.context.user as
                            | SuperAdmin
                            | Secretary).getCustomer(appointment.patient),
                          (this.context.user as
                            | SuperAdmin
                            | Secretary).getCustomer(appointment.therapist),
                          (this.context.user as
                            | SuperAdmin
                            | Secretary).getLocation(
                            appointment.branchOffice as Number
                          ),
                        ]);
                        return {
                          ...appointment,
                          patient: `${(user as ICustomer).first_name} ${
                            (user as ICustomer).last_name
                          }`,
                          therapist: `${(therapist as ICustomer).first_name} ${
                            (therapist as ICustomer).last_name
                          }`,
                          branchOffice: branch.shortName,
                          status: this.getStatus(appointment.status),
                        };
                      })
                    );
                    return data;
                  }}
                />
              </Grid>
            </Filter>
          </Grid>
        </Grid>
        <Calendar
          onViewChange={this.onViewChange}
          events={this.state.events}
          handleDateClick={() => {}}
          eventClick={(args: {
            el: HTMLElement;
            event: EventApi;
            jsEvent: MouseEvent;
            view: View;
          }) => {
            this.getAppointment(args.event.id);
          }}
        />
      </div>
    );
  }
}
export default withRouter(withAlert(Appointment) as any);
