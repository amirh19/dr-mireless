import React from "react";
import {
  Container,
  Grid,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  Typography,
  Chip,
} from "@material-ui/core";
import Table from "../../Components/DataVisual/Table";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import UserContext from "../../Context/User";
import IPayment from "../../Classes/Interfaces/IPayment";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import Pagination from "../../Components/Buttons/Pagination";
import Filter from "../../Components/ActionModals/Filter";
import ExportXSLX from "../../Components/DataVisual/ExportXSLX";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import moment from "moment";
import SearchUser from "../../Components/Search/User";
import { Delete } from "@material-ui/icons";
import Avatar from "@material-ui/core/Avatar";
import AddIcon from "@material-ui/icons/Add";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface PaymentsProps extends withAlertProps {}
interface PaymentsState {
  userFilter: ICustomer;
  start_range: String;
  end_range: String;
  payments: IPayment[];
  pagination: any;
}
class Payments extends React.Component<
  PaymentsProps & RouteComponentProps,
  PaymentsState
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    userFilter: {
      id: 0,
      first_name: "",
      last_name: "",
      birthday: "",
      email: "",
      phone: "",
      gender: 1,
    },
    start_range: moment().startOf("month").format("YYYY-MM-DD"),
    end_range: moment().format("YYYY-MM-DD"),
    payments: [],
    pagination: {
      first: 0,
      prev: 0,
      last: 0,
      next: 0,
    },
  };
  componentDidMount() {
    this.getPayments();
  }
  getPayments = async (page: Number = 1) => {
    const user = this.context.user as Secretary | SuperAdmin;
    const paymentsResponse = await user.getPayments(
      page,
      this.state.start_range,
      this.state.end_range,
      this.state.userFilter.id !== 0 ? this.state.userFilter.id : undefined
    );
    return paymentsResponse
      ? this.setState({
          payments: (paymentsResponse as { data: IPayment[]; pagination: any })
            .data,
          pagination: (paymentsResponse as {
            data: IPayment[];
            pagination: any;
          }).pagination,
        })
      : this.props.showError();
  };

  getPaymentType = (type: Number): String => {
    switch (type) {
      case 1:
        return "Efectivo";
      case 2:
        return "Tarjeta Débito";
      case 3:
        return "Tarjeta Crédito";
      case 4:
        return "Cheque";
      case 5:
        return "Cortesía";
      case 6:
        return "Otro";
      default:
        return "Sin datos";
    }
  };

  getServiceType = (type: Number): String => {
    switch (type) {
      case 1:
        return "Primera vez";
      case 2:
        return "Subsecuente";
      default:
        return "Sin datos";
    }
  };

  render() {
    return (
      <Container maxWidth="lg">
        <Grid container>
          <Grid container justify="flex-start" item sm={2} xs={12}>
            <Filter>
              <Grid container>
                <Grid item xs={12}>
                  <SearchUser
                    onAddUser={(user: ICustomer) =>
                      this.setState({ userFilter: user }, () =>
                        this.getPayments(1)
                      )
                    }
                  />
                </Grid>
                {this.state.userFilter.id !== 0 && (
                  <Grid item xs={12}>
                    <Chip
                      label={`${this.state.userFilter.first_name} ${this.state.userFilter.last_name}`}
                      deleteIcon={<Delete />}
                      onDelete={() =>
                        this.setState(
                          {
                            userFilter: {
                              id: 0,
                              first_name: "",
                              last_name: "",
                              birthday: "",
                              email: "",
                              phone: "",
                              gender: 1,
                            },
                          },
                          () => this.getPayments(1)
                        )
                      }
                    />
                  </Grid>
                )}
                <Grid item xs={12}>
                  <ExportXSLX
                    title="Pagos"
                    col_titles={[
                      "ID",
                      "PACIENTE",
                      "FECHA",
                      "MÉTODO DE PAGO",
                      "FACTURADO",
                      "CANTIDAD",
                      "SERVICIO",
                      "CONCEPTO",
                      "SUCURSAL",
                    ]}
                    withDateRange
                    onStartChange={(date: MaterialUiPickersDate) => {
                      this.setState(
                        {
                          start_range: moment(date).format("YYYY-MM-DD"),
                        },
                        () => this.getPayments(1)
                      );
                    }}
                    onEndChange={(date: MaterialUiPickersDate) => {
                      this.setState(
                        {
                          end_range: moment(date).format("YYYY-MM-DD"),
                        },
                        () => this.getPayments(1)
                      );
                    }}
                    start={moment(this.state.start_range).toDate()}
                    end={moment(this.state.end_range).toDate()}
                    endpoint={`v1/api/payment/?branchOffice=${
                      (this.context.user as Secretary | SuperAdmin).location_id
                        ? (this.context.user as Secretary | SuperAdmin)
                            .location_id
                        : ""
                    }&user=${
                      this.state.userFilter.id !== 0
                        ? this.state.userFilter.id
                        : ""
                    }`}
                    property="payments"
                    extraProcess={async (payments: IPayment[]) => {
                      let data = [];
                      data = await Promise.all(
                        payments.map(async (payment: IPayment) => {
                          const user = await (this.context.user as
                            | SuperAdmin
                            | Secretary).getCustomer(payment.user as Number);
                          const branch = await (this.context.user as
                            | SuperAdmin
                            | Secretary).getLocation(
                            payment.branchOffice as Number
                          );
                          return {
                            ...payment,
                            paymentType: this.getPaymentType(
                              payment.paymentType
                            ),
                            service: this.getServiceType(payment.service),
                            user: `${(user as ICustomer).first_name} ${
                              (user as ICustomer).last_name
                            }`,
                            branchOffice: branch.shortName,
                          };
                        })
                      );
                      return data;
                    }}
                  />
                </Grid>
              </Grid>
            </Filter>
            <Avatar
              style={{
                backgroundColor: "var(--principal)",
                marginBottom: "2rem",
                cursor: "pointer",
              }}
              onClick={() => this.props.history.push("/registrar-pagos/")}
            >
              <AddIcon />
            </Avatar>
          </Grid>
          <Grid item xs={12}>
            <Table
              model="ID"
              attributes={[
                "Paciente",
                "Fecha",
                "Método de pago",
                "Facturado",
                "Cantidad",
                "Servicio",
                "Concepto",
                "Sucursal",
              ]}
              data={this.state.payments.map((payment: IPayment) => ({
                name: payment.id?.toString() as string,
                values: [
                  payment.user,
                  moment(payment.date as string).format("DD-MM-YYYY"),
                  this.getPaymentType(payment.paymentType),
                  payment.billed ? "Sí" : "No",
                  `$ ${payment.quantity.toLocaleString("en-US")}`,
                  this.getServiceType(payment.service),
                  payment.concept,
                  payment.branchOffice,
                ],
              }))}
            />
          </Grid>
        </Grid>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getPayments}
            />
          </Grid>
        </Grid>
      </Container>
    );
  }
}
export default withRouter(withAlert(Payments));
