import React from "react";
import { Typography, TextField, Grid, Chip, Button } from "@material-ui/core";
import Upload from "../../Components/Buttons/Upload";
import Delete from "@material-ui/icons/Delete";
import ClinicalHistory from "../../Components/Form/ClinicalHistory";
import { withRouter, RouteComponentProps } from "react-router-dom";
import UserContext from "../../Context/User";
import Terapist from "../../Classes/Terapist";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";

const styles = {
  bold: {
    fontWeight: "bold",
    marginTop: "0.5rem",
    marginBottom: "0.5rem",
  },
};

const ClinicalConsulting = (
  props: RouteComponentProps<{ patient: string }> & withAlertProps
) => {
  const [files, setFiles] = React.useState<FileList | null>(null);
  const userContext = React.useContext<UserContext>(UserContext);
  const [patientName, setName] = React.useState<String>("");

  React.useEffect(() => {
    const getUser = async () => {
      const worker = userContext.user as Terapist;
      const response = await worker.getCustomer(
        parseInt(props.match.params.patient)
      );
      if (response) {
        setName(
          `${(response as ICustomer).first_name} ${
            (response as ICustomer).last_name
          }`
        );
      }
    };
    getUser();
  }, []);
  const submit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const worker: Terapist = userContext.user as Terapist;
    let data = new FormData(e.target as HTMLFormElement);
    data.append("date", new Date().toISOString().substr(0, 10));
    data.append("user", props.match.params.patient);
    data.append("branchOffice", (worker.location_id as Number).toString());
    if (files && files.length > 0) {
      data.append("study_result", files[0]);
    }
    const medicalConsultation = await worker.createMedicalConsultation(data);
    if (medicalConsultation) {
      props.showAlert({
        message: "Se ha agregado la consulta al historial",
        severity: "success",
      });
      props.history.push("/citas");
    } else {
      props.showError();
    }
  };

  return (
    <>
      <Grid container>
        <Grid item xs={12}>
          <Typography
            style={{ ...(styles.bold as any), marginBottom: "1.5rem" }}
            variant="h6"
          >
            Nombre del paciente: {patientName}
          </Typography>
        </Grid>
      </Grid>
      <ClinicalHistory customer_id={parseInt(props.match.params.patient)} />
      <form onSubmit={submit} style={{ marginTop: "1rem" }}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography style={styles.bold as any} variant="h6">
              Al enviar el formulario se creará un nuevo archivo al historial
              clínico del paciente.
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <TextField
              style={{ width: "100%" }}
              label="Exploración física"
              name="physical_exploration"
              multiline
              rows={3}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              style={{ width: "100%" }}
              label="Descripción de resultados"
              name="study_description"
              multiline
              rows={3}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              style={{ width: "100%" }}
              label="Diagnóstico"
              name="diagnosis"
              multiline
              rows={3}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              style={{ width: "100%" }}
              label="Observaciones"
              name="observations"
              multiline
              rows={3}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography
              style={{ ...(styles.bold as any), marginBottom: "0.5rem" }}
              variant="h6"
            >
              Resultados de estudio
            </Typography>
            {files
              ? Array.from(files as FileList).map((file) => (
                  <Chip
                    style={{ margin: "1rem" }}
                    label={file.name}
                    onDelete={() => {
                      setFiles(null);
                    }}
                    deleteIcon={<Delete />}
                  />
                ))
              : null}
            <Upload
              name="temp_files"
              accept="application/pdf"
              multiple={false}
              text="Subir archivo"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setFiles(event.target.files as FileList);
              }}
              style={{ backgroundColor: "var(--principal)" }}
            />
          </Grid>
          <Grid container justify="center">
            <Grid item xs={6}>
              <Button
                style={{
                  width: "100%",
                  backgroundColor: "var(--principal)",
                  color: "white",
                }}
                type="submit"
              >
                Terminar consulta
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </>
  );
};

export default withRouter(withAlert(ClinicalConsulting) as any);
