import React from "react";
import { Typography, Chip, Grid, Container, Button } from "@material-ui/core";
import IAudio from "../../Classes/Interfaces/IAudio";
import SuperAdmin from "../../Classes/SuperAdmin";
import Staff from "../../Classes/Staff";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import { withRouter, RouteComponentProps } from "react-router-dom";
import UserContext from "../../Context/User";
import CloseIcon from "@material-ui/icons/Close";
import SearchUser from "../../Components/Search/User";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import { Delete } from "@material-ui/icons";

interface DiseaseAudios {
  id: Number;
  disease: Number;
  audio: IAudio[];
}

interface State {
  diseaseName: String;
  diseaseAudios: IAudio[];
  user: ICustomer;
}

interface Props extends withAlertProps {}

class TherapyAssignment extends React.Component<
  Props & RouteComponentProps<{ id: string }>,
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;

  state = {
    diseaseName: "",
    diseaseAudios: [],
    user: {
      id: 0,
      first_name: "",
      last_name: "",
      email: "",
      phone: "",
      gender: 0,
      birthday: "",
    },
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    if (!id) {
      return this.props.history.push("/enfermedades");
    }
    this.getAudios(parseInt(id));
  }

  getAudios = async (id: Number) => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.getDiseaseAudios(id);
    const name = await worker.getDiseaseName(id);
    if (response && (response as DiseaseAudios[]).length > 0) {
      this.setState({
        diseaseName: name,
        diseaseAudios: (response as DiseaseAudios[])[0].audio,
      });
    } else {
      this.props.showError();
    }
  };

  create = async () => {
    if (this.state.user.id === 0) {
      return this.props.showAlert({
        message: "Debe seleccionar un paciente",
        severity: "info",
      });
    }
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.createTreatment({
      users: [this.state.user.id],
      name: this.state.diseaseName,
      audio: this.state.diseaseAudios.map((audio: IAudio) => audio.id),
    });
    return response
      ? this.props.history.push("/enfermedades")
      : this.props.showError();
  };

  render() {
    return (
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <SearchUser
              onAddUser={(user: ICustomer) => {
                this.setState({ user: user });
              }}
            />
          </Grid>
          <Grid item md={6} xs={12}>
            {this.state.user.first_name && (
              <Chip
                label={`${this.state.user.first_name} ${this.state.user.last_name}`}
                deleteIcon={<Delete />}
                onDelete={() =>
                  this.setState({
                    user: {
                      id: 0,
                      first_name: "",
                      last_name: "",
                      email: "",
                      phone: "",
                      gender: 0,
                      birthday: "",
                    },
                  })
                }
              />
            )}
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">
              A continuación se presentan los audios asignados a la enfermedad,
              si es necesario, elimine las biofrecuencias que no deben ser
              asignadas al paciente.
            </Typography>
          </Grid>
          <Grid item container direction="column" spacing={2} xs={4}>
            {this.state.diseaseAudios.map((audio: IAudio, index: number) => (
              <Chip
                style={{
                  marginBottom: 5,
                }}
                label={audio.name}
                onDelete={() => {
                  const { diseaseAudios } = this.state;
                  diseaseAudios.splice(index, 1);
                  this.setState({ diseaseAudios: diseaseAudios });
                }}
                deleteIcon={<CloseIcon />}
              />
            ))}
          </Grid>
          <Grid container justify="center" item xs={12}>
            <Button onClick={this.create} variant="contained" color="primary">
              Crear terapia
            </Button>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default withRouter(withAlert(TherapyAssignment));
