import React, { ChangeEvent } from "react";
import LocationForm from "../../Components/Form/Location";
import Add from "../../Components/ActionModals/Add";
import { Button, ButtonGroup, Avatar, Grid, Tooltip } from "@material-ui/core";
import Table from "../../Components/DataVisual/Table";
import EditIcon from "@material-ui/icons/Edit";
import SuperAdmin from "../../Classes/SuperAdmin";
import ILocation from "../../Classes/Interfaces/ILocation";
import UserContext from "../../Context/User";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import withAlert from "../../Components/HOC/withAlert";
import Pagination from "../../Components/Buttons/Pagination";

interface State extends ILocation {
  open: Boolean;
  edit: Boolean;
  locations: Array<ILocation>;
  pagination: any;
}
class Location extends React.Component<
  { showAlert: (alert: AlertMessage) => void; showError: Function },
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    id: 0,
    name: "",
    shortName: "",
    address: "",
    office_number: 0,
    phone_number: "",
    city: "",
    state: "",
    postalCode: "",
    open: false,
    edit: false,
    locations: [],
    pagination: {
      first: 0,
      last: 0,
      next: 0,
      prev: 0,
    },
  };
  componentDidMount() {
    this.getLocations();
  }

  getLocations = async (page: Number = 1) => {
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    const response: any = await admin.getLocations(page);
    this.setState({
      locations: response.data,
      pagination: response.pagination,
    });
  };

  addLocation = async () => {
    const {
      name,
      shortName,
      address,
      office_number,
      phone_number,
      city,
      state,
      postalCode,
    } = this.state;
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    const newLocation = await admin.addLocation({
      id: 0,
      name: name,
      shortName: shortName,
      address: address,
      office_number: office_number,
      phone_number: phone_number,
      city: city,
      state: state,
      postalCode: postalCode,
    });
    if (newLocation) {
      const { locations } = this.state as any;
      locations.push(newLocation);
      this.setState({ locations: locations, open: false, edit: false }, () =>
        this.props.showAlert({
          message: "Se ha agregado una sucursal",
          severity: "success",
        })
      );
    } else {
      this.props.showError();
    }
  };

  handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const key = event.currentTarget.name;
    if (Object.keys(this.state).includes(key)) {
      this.setState(({ [key]: event.currentTarget.value } as unknown) as Pick<
        ILocation,
        keyof ILocation
      >);
    }
  };

  updateLocation = async () => {
    const admin: SuperAdmin = this.context.user as SuperAdmin;
    const location: ILocation = { ...this.state };
    const updatedLocation = await admin.updateLocation(location);
    if (updatedLocation) {
      const { locations } = this.state as any;
      const updated = locations.map((branchOffice: ILocation) => {
        if (branchOffice.id === updatedLocation.id) {
          return updatedLocation;
        } else {
          return branchOffice;
        }
      });

      this.setState({ locations: updated, open: false, edit: false }, () =>
        this.props.showAlert({
          message: "Se ha actualizado una sucursal",
          severity: "success",
        })
      );
    } else {
      this.props.showError();
    }
  };

  render() {
    return (
      <>
        <div>
          <Add
            handleModal={() => {
              this.setState({
                id: 0,
                name: "",
                shortName: "",
                address: "",
                office_number: 0,
                phone_number: "",
                city: "",
                state: "",
                postalCode: "",
                open: !this.state.open,
                edit: false,
              });
            }}
            open={this.state.open}
          >
            <>
              <LocationForm handleChange={this.handleChange} {...this.state} />
              <ButtonGroup
                style={{
                  marginTop: "2rem",
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                  position: "absolute",
                  bottom: 10,
                }}
                size="large"
                color="default"
              >
                <Button
                  onClick={
                    this.state.edit ? this.updateLocation : this.addLocation
                  }
                >
                  {this.state.edit ? "Actualizar" : "Agregar"}
                </Button>
                <Button
                  onClick={() => this.setState({ open: false, edit: false })}
                >
                  Cancelar
                </Button>
              </ButtonGroup>
            </>
          </Add>
          <Table
            model="ID"
            attributes={[
              "Sucursal",
              "Nombre corto",
              "Teléfono",
              "Dirección",
              "Ciudad",
              "Estado",
              "Código Postal",
              "Acciones",
            ]}
            data={this.state.locations.map((location: ILocation): {
              name: String;
              values: any;
            } => ({
              name: location.id.toString(),
              values: [
                location.name,
                location.shortName,
                location.phone_number,
                location.address,
                location.city,
                location.state,
                location.postalCode,
                <Grid container direction="row" justify="flex-start">
                  <Tooltip title="Editar">
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--secondary)",
                      }}
                      onClick={() =>
                        this.setState({ ...location, open: true, edit: true })
                      }
                    >
                      <EditIcon />
                    </Avatar>
                  </Tooltip>
                </Grid>,
              ],
            }))}
          />
        </div>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getLocations}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withAlert(Location);
