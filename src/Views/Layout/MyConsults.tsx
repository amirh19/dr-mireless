import React from "react";
import {
  Grid,
  Typography,
  Chip,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from "@material-ui/core";
import Table from "../../Components/DataVisual/Table";
import UserContext from "../../Context/User";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import Pagination from "../../Components/Buttons/Pagination";
import Terapist from "../../Classes/Terapist";
import IMedicalConsultation from "../../Classes/Interfaces/IMedicalConsultation";
import SearchUser from "../../Components/Search/User";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import { Delete } from "@material-ui/icons";
import Filter from "../../Components/ActionModals/Filter";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import moment from "moment";
import ITerapist from "../../Classes/Interfaces/ITerapist";
import ExportXSLX from "../../Components/DataVisual/ExportXSLX";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";

interface State {
  open: Boolean;
  edit: Boolean;
  pagination: any;
  consults: IMedicalConsultation[];
  user: ICustomer;
  start_range: String;
  end_range: String;
  therapist: Number;
  terapists: ITerapist[];
  tpagination: any;
}

class MyConsults extends React.Component<withAlertProps, State> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    open: false,
    edit: false,
    pagination: { first: 0, last: 0, next: 0, prev: 0 },
    consults: [],
    user: {
      id: 0,
      first_name: "",
      last_name: "",
      email: "",
      phone: "",
      gender: 0,
      birthday: "",
    },
    start_range: moment().startOf("month").format("YYYY-MM-DD"),
    end_range: moment().format("YYYY-MM-DD"),
    therapist: -1,
    terapists: [],
    tpagination: { first: 0, last: 0, next: 0, prev: 0 },
  };

  componentDidMount() {
    this.getConsults();
    if (
      this.context.user instanceof Secretary ||
      this.context.user instanceof SuperAdmin
    ) {
      this.getTerapists();
    }
  }

  getTerapists = async (page: Number = 1) => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.getTerapists(page);
    if (response) {
      this.setState({
        terapists:
          response.data.length > 0
            ? response.data.map((relation: any) => relation.user)
            : [],
        tpagination: response.pagination,
      });
    } else {
      this.props.showError();
    }
  };

  getConsults = async (page: Number = 1) => {
    const worker = this.context.user as Terapist | Secretary | SuperAdmin;
    const responseConsults = await worker.getMedicalConsultations(
      page,
      this.state.start_range,
      this.state.end_range,
      this.state.user.id !== 0 ? this.state.user.id : undefined,
      this.context.user instanceof Terapist
        ? (this.context.user as Terapist).id
        : this.state.therapist !== -1
        ? this.state.therapist
        : undefined
    );
    if (responseConsults) {
      const consults = await Promise.all<IMedicalConsultation>(
        responseConsults.data.map(async (consult: IMedicalConsultation) => {
          const patient = await worker.getCustomer(consult.user);
          return {
            ...consult,
            user: `${(patient as ICustomer).first_name} ${
              (patient as ICustomer).last_name
            }`,
          };
        })
      );
      this.setState({
        consults: consults,
        pagination: ((responseConsults as unknown) as {
          data: IMedicalConsultation[];
          pagination: any;
        }).pagination,
      });
    } else {
      this.props.showError();
    }
  };

  render() {
    return (
      <>
        <Grid container>
          <Grid item xs={12}>
            <Filter>
              <Grid item xs={12}>
                <SearchUser
                  onAddUser={(user: ICustomer) => {
                    this.setState({ user: user }, () => this.getConsults(1));
                  }}
                />
              </Grid>
              {this.state.user.id !== 0 && (
                <Grid item xs={12}>
                  <Chip
                    label={`${this.state.user.first_name} ${this.state.user.last_name}`}
                    deleteIcon={<Delete />}
                    onDelete={() =>
                      this.setState(
                        {
                          user: {
                            id: 0,
                            first_name: "",
                            last_name: "",
                            email: "",
                            phone: "",
                            gender: 0,
                            birthday: "",
                          },
                        },
                        () => this.getConsults(1)
                      )
                    }
                  />
                </Grid>
              )}
              {!(this.context.user instanceof Terapist) && (
                <Grid item xs={12}>
                  <ExpansionPanel>
                    <ExpansionPanelSummary>
                      <Typography
                        style={{
                          width: "100%",
                          textAlign: "center",
                          fontWeight: "bold",
                          color: "var(--principal)",
                        }}
                        variant="body1"
                      >
                        Filtrar por terapeuta
                      </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails
                      style={{ display: "flex", flexDirection: "column" }}
                    >
                      <div
                        style={{
                          width: "100%",
                          maxHeight: "70%",
                          overflowY: "auto",
                        }}
                      >
                        <Chip
                          style={{
                            width: "100%",
                            paddingRight: "1rem",
                            paddingLeft: "1rem",
                            marginBottom: "1rem",
                            textAlign: "center",
                            backgroundColor:
                              this.state.therapist === -1
                                ? "var(--principal)"
                                : "white",
                            color:
                              this.state.therapist === -1 ? "white" : "black",
                          }}
                          label="Todos"
                          onClick={() => {
                            this.setState({ therapist: -1 }, () =>
                              this.getConsults(1)
                            );
                          }}
                        />
                        {this.state.terapists.map((terapist: ITerapist) => (
                          <Chip
                            style={{
                              width: "100%",
                              paddingRight: "1rem",
                              paddingLeft: "1rem",
                              marginBottom: "1rem",
                              textAlign: "center",
                              backgroundColor:
                                this.state.therapist === terapist.id
                                  ? "var(--principal)"
                                  : "white",
                              color:
                                this.state.therapist === terapist.id
                                  ? "white"
                                  : "black",
                            }}
                            label={`${terapist.first_name} ${terapist.last_name}`}
                            onClick={() =>
                              this.setState({ therapist: terapist.id }, () =>
                                this.getConsults()
                              )
                            }
                          />
                        ))}
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          justifyContent: "space-around",
                        }}
                      >
                        <Pagination
                          pagination={this.state.tpagination}
                          getData={this.getTerapists}
                        />
                      </div>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                </Grid>
              )}
              <Grid item xs={12} container justify="flex-end">
                <ExportXSLX
                  col_titles={[
                    "ID",
                    "PACIENTE",
                    "TERAPEUTA",
                    "EXPLORACIÓN FÍSICA",
                    "RESULTADO DEL ESTUDIO",
                    "DESCRIPCIÓN DEL ESTUDIO",
                    "DIAGNÓSTICO",
                    "OBSERVACIONES",
                    "FECHA",
                    "OFICINA",
                  ]}
                  withDateRange
                  onStartChange={(date: MaterialUiPickersDate) => {
                    this.setState(
                      {
                        start_range: moment(date).format("YYYY-MM-DD"),
                      },
                      this.getConsults
                    );
                  }}
                  onEndChange={(date: MaterialUiPickersDate) => {
                    this.setState(
                      {
                        end_range: moment(date).format("YYYY-MM-DD"),
                      },
                      this.getConsults
                    );
                  }}
                  start={moment(this.state.start_range).toDate()}
                  end={moment(this.state.end_range).toDate()}
                  endpoint={`v1/api/medical-consultation/?owner=${
                    this.context.user instanceof Terapist
                      ? (this.context.user as Terapist).id
                      : this.state.therapist !== -1
                      ? this.state.therapist
                      : ""
                  }&user=${this.state.user.id !== 0 ? this.state.user.id : ""}`}
                  title="Consultas"
                  extraProcess={async (consults: IMedicalConsultation[]) => {
                    let data = [];
                    data = await Promise.all(
                      consults.map(async (consult: IMedicalConsultation) => {
                        const [therapist, customer] = await Promise.all([
                          (this.context.user as
                            | SuperAdmin
                            | Terapist).getCustomer(consult.owner as Number),
                          (this.context.user as
                            | SuperAdmin
                            | Terapist).getCustomer(consult.user as Number),
                        ]);
                        const owner = new Terapist(
                          (therapist as unknown) as ITerapist
                        );
                        await owner.setLocation();
                        return {
                          ...consult,
                          owner: `${(therapist as ICustomer).first_name} ${
                            (therapist as ICustomer).last_name
                          }`,
                          user: `${(customer as ICustomer).first_name} ${
                            (customer as ICustomer).last_name
                          }`,
                          branchOffice: owner.location_name,
                        };
                      })
                    );
                    return data;
                  }}
                />
              </Grid>
            </Filter>
          </Grid>
          {this.state.user.id !== 0 && (
            <Grid item xs={12}>
              <Chip
                label={`${this.state.user.first_name} ${this.state.user.last_name}`}
                deleteIcon={<Delete />}
                onDelete={async () => {
                  await this.getConsults(1);
                  this.setState({
                    user: {
                      id: 0,
                      first_name: "",
                      last_name: "",
                      email: "",
                      phone: "",
                      gender: 0,
                      birthday: "",
                    },
                  });
                }}
              />
            </Grid>
          )}
        </Grid>
        <div style={{ width: "100vw", overflowX: "auto" }}>
          <Table
            model={"ID"}
            attributes={[
              "Paciente",
              "Fecha",
              "Exploración física",
              "Resultado de estudio",
              "Descripción del estudio",
              "Diagnóstico",
              "Observaciones",
            ]}
            data={this.state.consults.map((consult: IMedicalConsultation) => ({
              name: (consult.id as Number).toString(),
              values: [
                consult.user,
                moment(consult.date).format("DD-MM-YYYY"),
                consult.physical_exploration,
                consult.study_result ? (
                  <a target="__blank" href={consult.study_result as string}>
                    Ver archivo
                  </a>
                ) : (
                  "Sin archivo"
                ),
                consult.study_description,
                consult.diagnosis,
                consult.observations,
              ],
            }))}
          />
        </div>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getConsults}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withAlert(MyConsults);
