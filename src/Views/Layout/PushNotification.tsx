import React from "react";
import {
  ButtonGroup,
  Button,
  Grid,
  Select,
  TextField,
  Container,
  Typography,
  Chip,
} from "@material-ui/core";
import Add from "../../Components/ActionModals/Add";
import Pagination, {
  PaginationProps,
} from "../../Components/Buttons/Pagination";
import UserContext from "../../Context/User";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import INotification from "../../Classes/Interfaces/INotification";
import Table from "../../Components/DataVisual/Table";
import DoneAllIcon from "@material-ui/icons/DoneAll";
import CloseIcon from "@material-ui/icons/Close";
import { withRouter, RouteComponentProps } from "react-router-dom";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import IDisease from "../../Classes/Interfaces/IDisease";
import { Delete } from "@material-ui/icons";
import IAppointment from "../../Classes/Interfaces/IAppointment";
import SearchUser from "../../Components/Search/User";
import ICustomer from "../../Classes/Interfaces/ICustomer";

interface Props extends withAlertProps {}

interface State {
  type: Number;
  open: boolean;
  users: ICustomer[];
  title: String;
  body: String;
  extra?: Number;
  notitication_type: Number;
  created_at?: String;
  read?: Boolean;
  pagination: PaginationProps["pagination"];
  notifications: INotification[];
  diseases: IDisease[];
  dpagination: PaginationProps["pagination"];
  disease: IDisease;
}

class PushNotification extends React.Component<
  Props & RouteComponentProps<{ notification_type?: string; id?: string }>,
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;

  state = {
    type: 2,
    open: false,
    pagination: {
      first: 0,
      next: 0,
      prev: 0,
      last: 0,
    },
    notifications: [],
    diseases: [],
    dpagination: {
      first: 0,
      next: 0,
      prev: 0,
      last: 0,
    },
    disease: {
      id: 0,
      name: "",
      audios: [],
    },
    users: [],
    title: "",
    body: "",
    extra: 0,
    notitication_type: 0,
  };

  componentDidMount() {
    if (this.props.match.params.notification_type) {
      this.setState({
        type: parseInt(this.props.match.params.notification_type),
        open: true,
      });
    }
    this.getNotifications();
    this.getDiseases();
  }

  handleModal = () => {
    if (this.state.type === 1) {
      this.props.history.push("/notificaciones");
    }
    return this.setState((state) => ({
      open: !state.open,
      type: 2,
      disease: {
        id: 0,
        name: "",
        audios: [],
      },
      users: [],
      title: "",
      body: "",
      extra: 0,
      notitication_type: 0,
    }));
  };

  addNotification = async () => {
    let notification: INotification = {
      id: 0,
      title: this.state.title,
      body: this.state.body,
      notitication_type: this.state.type,
    };
    switch (this.state.type) {
      case 1:
        const appointmentId = this.props.match.params.id as string;
        const worker = this.context.user as Secretary | SuperAdmin;
        const appointment: IAppointment = await worker.getAppointment(
          appointmentId
        );
        notification.extra = appointment.id as Number;
        notification.users = `[${appointment.patient}]`;
        break;
      case 3:
        notification.extra = this.state.disease.id;
        break;
      case 4:
        notification.extra = 5;
        break;
      case 2:
      case 5:
      case 6:
        notification.users = `[${this.state.users
          .map((user: ICustomer) => user.id)
          .join(",")}]`;
        break;
      case 7:
        break;
    }
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.addNotification(notification);
    if (response) {
      const { notifications } = this.state;
      notifications.push(response as never);
      this.setState({ notifications: notifications, open: false });
    }
  };

  getNotifications = async (page: Number = 1) => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.getAllNotifications(page);
    return response
      ? this.setState({
          notifications: (response as { data: any; pagination: any }).data,
          pagination: (response as { data: any; pagination: any }).pagination,
        })
      : this.props.showError();
  };

  getDiseases = async (page: Number = 1) => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.getDiseases(page);
    return response
      ? this.setState({
          diseases: response.data,
          dpagination: response.pagination,
        })
      : this.props.showError();
  };

  render() {
    return (
      <>
        <div>
          <Add open={this.state.open} handleModal={this.handleModal}>
            <Container maxWidth="md">
              <Grid
                container
                justify="center"
                alignItems="center"
                style={{
                  overflowY: "auto",
                  overflowX: "hidden",
                  maxHeight: "60vh",
                  marginBottom: "5rem",
                }}
              >
                <Grid item xs={12} style={{ marginBottom: 20 }}>
                  <Select
                    fullWidth
                    native
                    value={this.state.type}
                    onChange={(
                      event: React.ChangeEvent<{
                        name?: string;
                        value: unknown;
                      }>
                    ) =>
                      this.setState({
                        type: parseInt(event.target.value as string),
                      })
                    }
                  >
                    <option value={1} disabled={this.state.type !== 1}>
                      Cita
                    </option>
                    <option value={3} disabled={this.state.type === 1}>
                      Enfermedad
                    </option>
                    <option value={4} disabled={this.state.type === 1}>
                      Vigencia
                    </option>
                    <option value={2} disabled={this.state.type === 1}>
                      Algunos usuarios
                    </option>
                    <option value={5} disabled={this.state.type === 1}>
                      Noticia
                    </option>
                    <option value={6} disabled={this.state.type === 1}>
                      Otro
                    </option>
                    <option value={7} disabled={this.state.type === 1}>
                      Todos los usuarios
                    </option>
                  </Select>
                </Grid>
                {this.state.type === 3 && this.state.disease.id === 0 && (
                  <Grid
                    item
                    xs={12}
                    style={{ height: "20vh", overflowY: "auto" }}
                  >
                    {this.state.diseases.map((disease: IDisease) => (
                      <Typography
                        onClick={() => this.setState({ disease: disease })}
                        style={{
                          backgroundColor: "var(--gray)",
                          borderRadius: 5,
                          marginBottom: 5,
                          padding: 5,
                          textAlign: "center",
                        }}
                        variant="body1"
                      >
                        {disease.name}
                      </Typography>
                    ))}
                    <Pagination
                      pagination={this.state.dpagination}
                      getData={this.getDiseases}
                    />
                  </Grid>
                )}
                {this.state.type === 3 && this.state.disease.id !== 0 && (
                  <Chip
                    label={this.state.disease.name}
                    deleteIcon={<Delete />}
                    onDelete={() =>
                      this.setState({
                        disease: { id: 0, name: "", audios: [] },
                      })
                    }
                  />
                )}
                <Grid item xs={12}>
                  {(this.state.type === 2 ||
                    this.state.type === 5 ||
                    this.state.type === 6) && (
                    <>
                      <SearchUser
                        onAddUser={(user: ICustomer) => {
                          const { users } = this.state;
                          users.push(user as never);
                          this.setState({ users: users }, () =>
                            this.props.showAlert({
                              message: `Se ha agregado el paciente ${user.first_name} ${user.last_name}`,
                              severity: "info",
                            })
                          );
                        }}
                      />
                      {this.state.users.map(
                        (user: ICustomer, index: number) => (
                          <Chip
                            label={`${user.first_name} ${user.last_name}`}
                            deleteIcon={<Delete />}
                            onDelete={() => {
                              const { users } = this.state;
                              users.splice(index, 1);
                              this.setState({ users: users });
                            }}
                          />
                        )
                      )}
                    </>
                  )}
                </Grid>
                <Grid item sm={6} xs={12}>
                  <TextField
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      this.setState({ title: e.target.value });
                    }}
                    fullWidth
                    label="Título"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      this.setState({ body: e.target.value });
                    }}
                    fullWidth
                    label="Mensaje"
                    multiline
                    rows={2}
                  />
                </Grid>
                <ButtonGroup
                  style={{
                    marginTop: "2rem",
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    position: "absolute",
                    bottom: 10,
                  }}
                  size="large"
                  color="default"
                >
                  <Button onClick={this.addNotification}>
                    Enviar notificación
                  </Button>
                  <Button onClick={this.handleModal}>Cancelar</Button>
                </ButtonGroup>
              </Grid>
            </Container>
          </Add>
        </div>
        <Table
          model="ID"
          attributes={[
            "Título",
            "Mensaje",
            "Leído",
            "Tipo",
            "Fecha de creación",
          ]}
          data={this.state.notifications.map((notification: INotification) => ({
            name: notification.id?.toString() as string,
            values: [
              notification.title,
              notification.body,
              notification.read ? (
                <DoneAllIcon style={{ color: "var(--principal)" }} />
              ) : (
                <CloseIcon />
              ),
              notification.notitication_type,
              notification.created_at,
            ],
          }))}
        />
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getNotifications}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withRouter(withAlert(PushNotification));
