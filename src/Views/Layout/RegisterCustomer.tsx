import React from "react";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Add from "../../Components/ActionModals/Add";
import UserContext from "../../Context/User";
import CustomerForm from "../../Components/Form/Customer";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import SuperAdmin from "../../Classes/SuperAdmin";
import Secretary from "../../Classes/Secretary";
import Table from "../../Components/DataVisual/Table";
import Avatar from "@material-ui/core/Avatar";
import EditIcon from "@material-ui/icons/Edit";
import IBillingData from "../../Classes/Interfaces/IBillingData";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import withAlert from "../../Components/HOC/withAlert";
import NewAppointment from "../../Components/Buttons/NewAppointment";
import Grid from "@material-ui/core/Grid";
import Profile from "../../Components/DataVisual/Profile";
import Pagination from "../../Components/Buttons/Pagination";
import moment from "moment";
import { Tooltip, Chip } from "@material-ui/core";
import SearchUser from "../../Components/Search/User";
import Filter from "../../Components/ActionModals/Filter";
import { Delete } from "@material-ui/icons";
import ExportXSLX from "../../Components/DataVisual/ExportXSLX";

interface State
  extends Omit<ICustomer, "birthday">,
    Omit<IBillingData, "id" | "email"> {
  birthday: Date;
  password: String;
  password_confirm: String;
  open: Boolean;
  edit: Boolean;
  pagination: any;
  customers: ICustomer[];
  billing_email: String;
  billing_id: Number;
  filtered: Boolean;
}
class RegisterCustomer extends React.Component<
  { showAlert: (alert: AlertMessage) => void; showError: Function },
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    id: 0,
    first_name: "",
    last_name: "",
    email: "",
    gender: 1,
    phone: "",
    birthday: moment().toDate(),
    password: "",
    password_confirm: "",
    open: false,
    edit: false,
    pagination: {
      first: 0,
      last: 0,
      next: 0,
      prev: 0,
    },
    customers: [],
    /**billing data */
    billing_id: 0,
    BusinessName: "",
    rfc: "",
    billing_email: "",
    address: "",
    city: "",
    state: "",
    cp: "",
    filtered: false,
  };

  async componentDidMount() {
    this.getCustomers();
  }

  getCustomers = async (page: Number = 1) => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.getCustomers(page);
    if (response) {
      this.setState({
        customers: response.data.map((relation: any) => relation.user),
        pagination: response.pagination,
      });
    } else {
      this.props.showError();
    }
  };

  handleModal = () => {
    this.setState((state) => ({
      id: 0,
      open: !state.open,
      edit: false,
      first_name: "",
      last_name: "",
      email: "",
      gender: 1,
      phone: "",
      birthday: moment().toDate(),
      password: "",
      password_confirm: "",
      /**billing data */
      billing_id: 0,
      BusinessName: "",
      rfc: "",
      billing_email: "",
      address: "",
      city: "",
      state: "",
      cp: "",
    }));
  };

  addCustomer = async () => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const response = await worker.addCustomer(
      {
        id: 0,
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        email: this.state.email,
        phone: this.state.phone,
        gender: this.state.gender,
        birthday: moment(this.state.birthday).format("YYYY-MM-DD"),
        password: this.state.password,
        password_confirm: this.state.password_confirm,
      },
      {
        BusinessName: this.state.BusinessName,
        rfc: this.state.rfc,
        email: this.state.billing_email,
        address: this.state.address,
        city: this.state.city,
        state: this.state.state,
        cp: this.state.cp,
      }
    );
    if (response) {
      const { customers } = this.state;
      customers.push(response as never);
      this.setState(
        {
          customers: customers,
          open: false,
          edit: false,
        },
        () =>
          this.props.showAlert({
            message: "Se ha agregado un Paciente",
            severity: "success",
          })
      );
    } else {
      this.props.showAlert({
        message: "Ha ocurrido un error, verifique los campos",
        severity: "error",
      });
    }
  };
  updateCustomer = async () => {
    const worker = this.context.user as Secretary | SuperAdmin;
    const customer = await worker.updateCustomer({
      id: this.state.id,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      email: this.state.email,
      phone: this.state.phone,
      gender: this.state.gender,
      birthday: moment(this.state.birthday).format("YYYY-MM-DD"),
    });
    const billingData =
      this.state.billing_id === 0
        ? await worker.createBilling(/*Customer id*/ this.state.id, {
            BusinessName: this.state.BusinessName,
            rfc: this.state.rfc,
            email: this.state.billing_email,
            address: this.state.address,
            city: this.state.city,
            state: this.state.state,
            cp: this.state.cp,
          })
        : await worker.updateBilling({
            id: this.state.billing_id,
            BusinessName: this.state.BusinessName,
            rfc: this.state.rfc,
            email: this.state.billing_email,
            address: this.state.address,
            city: this.state.city,
            state: this.state.state,
            cp: this.state.cp,
          });
    if (customer) {
      const { customers } = this.state;
      const updated: ICustomer[] = customers.map((client: ICustomer) => {
        if (client.id === (customer as ICustomer).id) {
          return customer as ICustomer;
        } else {
          return client;
        }
      });
      this.setState({ customers: updated, open: false, edit: false }, () =>
        this.props.showAlert({
          message: "Se ha actualizado la información",
          severity: "success",
        })
      );
    } else {
      this.props.showAlert({
        message: "Ocurrió un error al actualizar la información del usuario",
        severity: "error",
      });
    }
    if (!billingData) {
      this.props.showAlert({
        message: "Ocurrió un error al actualizar la información de facturación",
        severity: "error",
      });
    } else {
      this.props.showAlert({
        message: "Se actualizó la información de Facturación",
        severity: "success",
      });
    }
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const key = event.currentTarget.name;
    if (Object.keys(this.state).includes(key)) {
      if (key === "gender") {
        this.setState({ gender: parseInt(event.currentTarget.value) });
      } else {
        this.setState(({ [key]: event.currentTarget.value } as unknown) as Pick<
          State,
          keyof State
        >);
      }
    }
  };
  handleBirthdayChange = (date: any) => {
    this.setState({ birthday: date });
  };
  render() {
    return (
      <>
        <Grid container>
          <Grid item sm={1} xs={12}>
            <Add open={this.state.open} handleModal={this.handleModal}>
              <>
                <CustomerForm
                  {...this.state}
                  handleBirthdayChange={this.handleBirthdayChange}
                  handleChange={this.handleChange}
                />
                <ButtonGroup
                  style={{
                    marginTop: "2rem",
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                    position: "absolute",
                    bottom: 10,
                  }}
                  size="large"
                  color="default"
                >
                  <Button
                    onClick={
                      this.state.edit ? this.updateCustomer : this.addCustomer
                    }
                  >
                    {this.state.edit ? "Actualizar" : "Agregar"}
                  </Button>
                  <Button
                    onClick={() => this.setState({ open: false, edit: false })}
                  >
                    Cancelar
                  </Button>
                </ButtonGroup>
              </>
            </Add>
          </Grid>
          <Grid item sm={1} xs={12}>
            <Filter>
              <Grid container>
                <Grid item xs={12}>
                  <SearchUser
                    onAddUser={(user: ICustomer) =>
                      this.setState({
                        id: user.id,
                        customers: [user],
                        filtered: true,
                      })
                    }
                  />
                </Grid>
                <Grid item xs={12}>
                  <ExportXSLX
                    title="PACIENTES"
                    col_titles={[
                      "ID",
                      "NOMBRE",
                      "FECHA DE NACIMIENTO",
                      "SEXO",
                      "EMAIL",
                      "TELÉFONO",
                      "RFC",
                      "EMAIL FACTURACION",
                      "DIRECCION FACTURACION",
                      "CIUDAD",
                      "ESTADO",
                      "CÓDIGO POSTAL",
                    ]}
                    withDateRange={false}
                    endpoint={`v1/api/assign-branch-office/?branch_office=${
                      (this.context.user as Secretary | SuperAdmin).location_id
                    }&type=3&user=${this.state.id || ""}`}
                    extraProcess={async (assignOffice: any) => {
                      console.log(assignOffice);
                      let customers: ICustomer[] = assignOffice.map(
                        (relation: any) => relation.user
                      );
                      let data = [];
                      data = await Promise.all(
                        customers.map(async (customer: ICustomer) => {
                          const worker = this.context.user as
                            | Secretary
                            | SuperAdmin;
                          const billing = await worker.getBillingByUser(
                            customer.id
                          );
                          console.log(billing);
                          if (billing) {
                            const {
                              rfc,
                              email,
                              address,
                              city,
                              state,
                              cp,
                            } = billing as IBillingData;
                            return {
                              id: customer.id,
                              name: `${customer.first_name} ${customer.last_name}`,
                              birthday: customer.birthday,
                              gender:
                                customer.gender === 1 ? "HOMBRE" : "MUJER",
                              email: customer.email,
                              phone: customer.phone,
                              rfc: rfc,
                              email_rfc: email,
                              address: address,
                              city: city,
                              state: state,
                              cp: cp,
                            };
                          } else {
                            return {
                              id: customer.id,
                              name: `${customer.first_name} ${customer.last_name}`,
                              birthday: customer.birthday,
                              gender:
                                customer.gender === 1 ? "HOMBRE" : "MUJER",
                              email: customer.email,
                              phone: customer.phone,
                              rfc: "",
                              email_rfc: "",
                              address: "",
                              city: "",
                              state: "",
                              cp: "",
                            };
                          }
                        })
                      );
                      return data;
                    }}
                  />
                </Grid>
              </Grid>
            </Filter>
          </Grid>
          {this.state.filtered && (
            <Grid item xs={12}>
              <Chip
                label={`${(this.state.customers[0] as ICustomer).first_name} ${
                  (this.state.customers[0] as ICustomer).last_name
                }`}
                deleteIcon={<Delete />}
                onDelete={() =>
                  this.setState({ id: 0, filtered: false }, this.getCustomers)
                }
              />
            </Grid>
          )}
          <Grid item xs={12}>
            <Table
              model="ID"
              attributes={["Paciente", "Email", "Teléfono", "Edad", "Acciones"]}
              data={this.state.customers.map((customer: any): {
                name: String;
                values: any;
              } => ({
                name: customer.id,
                values: [
                  `${customer.first_name} ${customer.last_name}`,
                  customer.email,
                  customer.phone,
                  moment().diff(customer.birthday, "years", false),

                  <Grid container justify="center" direction="row">
                    <Tooltip title="Editar">
                      <Avatar
                        style={{
                          cursor: "pointer",
                          backgroundColor: "var(--secondary)",
                        }}
                        onClick={async () => {
                          const worker = this.context.user as
                            | Secretary
                            | SuperAdmin;
                          const billing = await worker.getBillingByUser(
                            customer.id
                          );
                          if (billing) {
                            const {
                              id,
                              email,
                              ...restBilling
                            } = billing as IBillingData;
                            const { birthday, ...restCustomer } = customer;
                            this.setState({
                              billing_id: id,
                              billing_email: email,
                              birthday: new Date(birthday),
                              ...restBilling,
                              ...restCustomer,
                              open: true,
                              edit: true,
                            });
                          } else {
                            const { birthday, ...restCustomer } = customer;
                            this.setState({
                              birthday: new Date(birthday),
                              ...restCustomer,
                              billing_id: 0,
                              open: true,
                              edit: true,
                            });
                          }
                        }}
                      >
                        <EditIcon />
                      </Avatar>
                    </Tooltip>
                    <NewAppointment userId={customer.id} />

                    <Profile customer_id={customer.id} />
                  </Grid>,
                ],
              }))}
            />
          </Grid>
        </Grid>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getCustomers}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withAlert(RegisterCustomer);
