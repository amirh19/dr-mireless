import React from "react";
import { withRouter, RouteComponentProps } from "react-router-dom";
import {
  Container,
  Grid,
  InputLabel,
  FormControl,
  Select,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  TextField,
  InputAdornment,
  Button,
  Chip,
} from "@material-ui/core";
import Checkbox, { CheckboxProps } from "@material-ui/core/Checkbox";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import UserContext from "../../Context/User";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import logobase64 from "../../assets/logobase64";
import * as jsPDF from "jspdf";
import SearchUser from "../../Components/Search/User";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import { Delete } from "@material-ui/icons";

interface Props extends withAlertProps {}

const useStyles = makeStyles(() => ({
  maxWidth: {
    width: "100%",
  },
}));

const BlueCheckbox = withStyles({
  root: {
    color: "var(--principal)",
    "&$checked": {
      color: "var(--principal)",
    },
  },
  checked: {},
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

const Payment: React.FC<Props & RouteComponentProps<{ patient: string }>> = (
  props: Props & RouteComponentProps<{ patient?: string }>
) => {
  const classes = useStyles();
  const [patient, setPatient] = React.useState<ICustomer>();
  const [quantity, setQuantity] = React.useState<Number>(0);
  const [paymentType, setPaymentType] = React.useState<Number>(1);
  const [billed, setBilled] = React.useState<Boolean>(false);
  const [service, setService] = React.useState<Number>(1);
  const [send, setSend] = React.useState<boolean>(false);
  const [concept, setConcept] = React.useState<String>("");
  const userContext = React.useContext<UserContext>(UserContext);

  React.useEffect(() => {
    const getPatient = async () => {
      const worker = userContext.user as SuperAdmin;
      const patient = await worker.getCustomer(
        parseInt(props.match.params.patient as string)
      );
      if (patient) {
        setPatient(patient as ICustomer);
      }
    };
    if (props.match.params.patient) {
      getPatient();
    }
  }, [props.match.params.patient]);

  const getPaymentType = (type: Number): String => {
    switch (type) {
      case 1:
        return "Efectivo";
      case 2:
        return "Tarjeta Débito";
      case 3:
        return "Tarjeta Crédito";
      case 4:
        return "Cheque";
      case 5:
        return "Cortesía";
      case 6:
        return "Otro";
      default:
        return "Sin datos";
    }
  };

  const getServiceType = (type: Number): String => {
    switch (type) {
      case 1:
        return "Primera vez";
      case 2:
        return "Subsecuente";
      default:
        return "Sin datos";
    }
  };

  const submit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const payment = {
      user: (patient as ICustomer).id,
      date: new Date().toISOString().substr(0, 10),
      paymentType: paymentType,
      billed: billed,
      quantity: quantity,
      service: service,
      branchOffice: 0,
      concept: concept,
    };
    if (
      await (userContext.user as Secretary | SuperAdmin).createPayment(payment)
    ) {
      setQuantity(0);
      setPaymentType(1);
      setBilled(false);
      setService(1);
      setConcept("");
      if (send) {
        try {
          const doc = new jsPDF();
          doc.setFillColor("#0077c8");
          doc.rect(0, 0, 500, 350, "F");
          doc.addImage(logobase64, "PNG", 15, 5, 125, 34);
          doc.setFontSize(10);
          doc.setTextColor("#ffffff");
          doc.text(170, 15, `Fecha: ${payment.date}`);
          doc.setFontSize(12);
          doc.text(
            15,
            50,
            `Nombre: ${patient?.first_name} ${patient?.last_name}`
          );
          doc.text(
            15,
            60,
            `Cantidad: $${payment.quantity.toLocaleString("en-US")}`
          );
          doc.text(
            80,
            60,
            `Método de pago: ${getPaymentType(payment.paymentType)}`
          );
          doc.text(15, 70, `Facturado: ${payment.billed ? "Sí" : "No"}`);
          doc.text(15, 80, `Servicio: ${getServiceType(payment.service)}`);
          doc.text(
            80,
            80,
            `Oficina :#${(userContext.user as Secretary | SuperAdmin).location_id || 0} ${
              (userContext.user as Secretary | SuperAdmin).location_name || ""
            }`
          );
          doc.text(
            15,
            90,
            `Concepto : ${concept}`
          );
          const blob = doc.output("blob");
          const data = new FormData();
          data.append("user", (patient as ICustomer).id.toString());
          data.append(
            "receipt",
            new File([blob], "RECIBO_CONSULTA.pdf", {
              type: "application/pdf",
            })
          );
          (userContext.user as Secretary | SuperAdmin).sendByEmail(data);
        } catch (error) {
          console.error(error);
        }
      }
      props.history.push("/pagos");
    } else {
      props.showError();
    }
  };
  return (
    <form onSubmit={submit}>
      <Container maxWidth="lg">
        <Grid container spacing={3}>
          <Grid item xs={12}>
            {!patient && (
              <SearchUser
                onAddUser={(patient) => {
                  setPatient(patient);
                }}
              />
            )}
            {patient && (
              <Chip
                label={`${patient.first_name} ${patient.last_name}`}
                deleteIcon={<Delete />}
                onDelete={() => setPatient(undefined)}
              />
            )}
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.maxWidth}
              name="quantity"
              label="Cantidad"
              type="number"
              value={quantity}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setQuantity(parseFloat(e.target.value));
              }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">$</InputAdornment>
                ),
              }}
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.maxWidth}
              name="concept"
              label="Concepto"
              multiline
              rows={3}
              value={concept}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setConcept(e.target.value);
              }}
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <FormControl className={classes.maxWidth}>
              <InputLabel id="paymentLabel">Método de pago</InputLabel>
              <Select
                labelId="paymentLabel"
                className={classes.maxWidth}
                native
                inputProps={{
                  name: "paymentType",
                }}
                value={paymentType}
                onChange={(
                  e: React.ChangeEvent<{ name?: string; value: unknown }>
                ) => {
                  setPaymentType(parseInt(e.target.value as string));
                }}
              >
                <option value={1}>Efectivo</option>
                <option value={2}>Tarjeta Débito</option>
                <option value={3}>Tarjeta Crédito</option>
                <option value={4}>Cheque</option>
                <option value={5}>Cortesía</option>
                <option value={6}>Otro</option>
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={3} xs={6}>
            <FormControl component="fieldset">
              <FormLabel component="legend">Facturado</FormLabel>
              <RadioGroup
                value={billed}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  setBilled(e.target.value === "true")
                }
                aria-label="billed"
                name="billed"
              >
                <FormControlLabel value={true} control={<Radio />} label="Sí" />
                <FormControlLabel
                  value={false}
                  control={<Radio />}
                  label="No"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item sm={6} xs={12}>
            <FormControl className={classes.maxWidth}>
              <InputLabel id="serviceType">Tipo de servicio</InputLabel>
              <Select
                labelId="serviceType"
                className={classes.maxWidth}
                native
                inputProps={{
                  name: "service",
                }}
                value={service}
                onChange={(
                  e: React.ChangeEvent<{ name?: string; value: unknown }>
                ) => {
                  setService(parseInt(e.target.value as string));
                }}
              >
                <option value={1}>Primera vez</option>
                <option value={2}>Subsecuente</option>
              </Select>
            </FormControl>
          </Grid>
          <Grid
            container
            direction="column"
            justify="center"
            item
            sm={6}
            xs={12}
          >
            <Button
              style={{
                backgroundColor: "var(--principal)",
                color: "white",
                opacity: patient ? 1 : 0.6,
              }}
              type="submit"
              disabled={!patient}
            >
              Registrar pago
            </Button>
            <FormControlLabel
              control={
                <BlueCheckbox
                  checked={send}
                  onChange={() => setSend(!send)}
                  name="sentToEmail"
                />
              }
              label="Enviar por correo electrónico"
            />
          </Grid>
        </Grid>
        <div id="canvas"></div>
      </Container>
    </form>
  );
};
export default withRouter(withAlert(Payment));
