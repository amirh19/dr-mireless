import React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Add from "../../Components/ActionModals/Add";
import UserContext from "../../Context/User";
import DiseaseForm from "../../Components/Form/Disease";
import IDisease from "../../Classes/Interfaces/IDisease";
import IAudio from "../../Classes/Interfaces/IAudio";
import SuperAdmin from "../../Classes/SuperAdmin";
import Staff from "../../Classes/Staff";
import Table from "../../Components/DataVisual/Table";
import Avatar from "@material-ui/core/Avatar";
import EditIcon from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import withAlert from "../../Components/HOC/withAlert";
import Confirmation from "../../Components/ActionModals/Confirmation";
import Pagination from "../../Components/Buttons/Pagination";
import Tooltip from "@material-ui/core/Tooltip";
import AssignmentIndIcon from "@material-ui/icons/AssignmentInd";
import { withRouter, RouteComponentProps } from "react-router-dom";

interface State extends IDisease {
  open: Boolean;
  confirm: Boolean;
  edit: Boolean;
  diseases: IDisease[];
  pagination: any;
  audioPagination: any;
  allAudios: IAudio[];
  toUpdate: Number;
  handleOk: Function;
}
class Disease extends React.Component<
  {
    showAlert: (alert: AlertMessage) => void;
    showError: Function;
  } & RouteComponentProps,
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    id: 0,
    name: "",
    audios: [],
    open: false,
    edit: false,
    diseases: [],
    pagination: {
      first: 0,
      last: 0,
      next: 0,
      prev: 0,
    },
    allAudios: [],
    audioPagination: {},
    toUpdate: 0,
    confirm: false,
    handleOk: () => {},
  };

  componentDidMount() {
    Promise.all([this.getDiseases(), this.getAudios()]);
  }

  getAudios = async (page: Number = 1) => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.getAudios(page);
    if (response) {
      this.setState({
        allAudios: response.data,
        audioPagination: response.pagination,
      });
    } else {
      this.props.showError();
    }
  };

  getDiseases = async (page: Number = 1) => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.getDiseases(page);
    if (response) {
      this.setState({
        diseases: response.data,
        pagination: response.pagination,
      });
    } else {
      this.props.showError();
    }
  };

  handleModal = () => {
    this.setState((state) => ({
      open: !state.open,
      edit: false,
      id: 0,
      name: "",
      audios: [],
    }));
  };

  addDisease = async () => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.addDisease({
      id: 0,
      name: this.state.name,
      audios: this.state.audios,
    });
    if (response) {
      const { diseases } = this.state;
      diseases.push(response as never);
      this.setState(
        {
          diseases: diseases,
          open: false,
        },
        () =>
          this.props.showAlert({
            message: "Se ha agregado una enfermedad",
            severity: "success",
          })
      );
    } else {
      this.props.showError();
    }
  };
  updateDisease = async () => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.updateDisease(
      {
        id: this.state.id,
        name: this.state.name,
        audios: this.state.audios,
      },
      this.state.toUpdate
    );
    if (response) {
      const { diseases } = this.state;
      const updated = diseases.map((disease: IDisease) => {
        if (disease.id === response.id) {
          return response;
        } else {
          return disease;
        }
      });
      this.setState({ diseases: updated, open: false }, () =>
        this.props.showAlert({
          message: "Se ha actualizado una enfermedad",
          severity: "success",
        })
      );
    } else {
      this.props.showError();
    }
  };

  deleteDisease = async (id: Number, index: Number) => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.deleteDisease(id);
    if (response) {
      const { diseases } = this.state;
      diseases.splice(index as number, 1);
      this.setState(
        { diseases: diseases, confirm: false, handleOk: () => {} },
        () =>
          this.props.showAlert({
            message: "Se ha eliminado una enfermedad",
            severity: "success",
          })
      );
    } else {
      this.props.showError();
    }
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const key = event.currentTarget.name;
    const currentTarget = event.currentTarget;
    if (Object.keys(this.state).includes(key)) {
      this.setState(({ [key]: currentTarget.value } as unknown) as Pick<
        State,
        keyof State
      >);
    }
  };
  handleEdit = async (disease: IDisease): Promise<any> => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.getDiseaseAudios(disease.id);
    if (response) {
      const audios: {
        id: Number;
        disease: Number;
        audio: IAudio[];
      }[] = response as { id: Number; disease: Number; audio: IAudio[] }[];
      this.setState({
        ...disease,
        toUpdate: audios.length > 0 ? audios[0].id : 0,
        audios: audios.length > 0 ? (audios[0].audio as IAudio[]) : [],
        edit: true,
        open: true,
      });
    } else {
      this.props.showError();
    }
  };
  render() {
    return (
      <>
        <div>
          <Add open={this.state.open} handleModal={this.handleModal}>
            <>
              <DiseaseForm
                {...this.state}
                handleChange={this.handleChange}
                addAudio={(audio: any) => {
                  const { audios } = this.state;
                  audios.push(audio as never);
                  this.setState({ audios: audios });
                }}
                deleteAudio={(index: number) => {
                  const { audios } = this.state;
                  audios.splice(index, 1);
                  this.setState({ audios: audios });
                }}
                getOrderedAudios={(audios: any) =>
                  this.setState({ audios: audios })
                }
                audioPagination={this.state.audioPagination}
                getPage={(page: Number) => this.getAudios(page)}
              />
              <ButtonGroup
                style={{
                  marginTop: "2rem",
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                }}
                size="large"
                color="default"
              >
                <Button
                  onClick={
                    this.state.edit ? this.updateDisease : this.addDisease
                  }
                >
                  {this.state.edit ? "Actualizar" : "Agregar"}
                </Button>
                <Button
                  onClick={() => this.setState({ open: false, edit: false })}
                >
                  Cancelar
                </Button>
              </ButtonGroup>
            </>
          </Add>
          <Table
            model="ID"
            attributes={["Enfermedad", "Acciones"]}
            data={this.state.diseases.map((disease: IDisease, index: Number): {
              name: String;
              values: any;
            } => ({
              name: disease.id.toString(),
              values: [
                disease.name,
                <Grid container direction="row" justify="flex-start">
                  <Tooltip title="Editar">
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--secondary)",
                      }}
                      onClick={() => this.handleEdit(disease)}
                    >
                      <EditIcon />
                    </Avatar>
                  </Tooltip>
                  <Tooltip title="Asignar a paciente">
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--secondary)",
                      }}
                      onClick={() =>
                        this.props.history.push(`/terapia/${disease.id}`)
                      }
                    >
                      <AssignmentIndIcon />
                    </Avatar>
                  </Tooltip>
                  <Tooltip title="Eliminar">
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--danger)",
                      }}
                      onClick={() =>
                        this.setState({
                          confirm: true,
                          handleOk: () => {
                            this.deleteDisease(disease.id, index);
                          },
                        })
                      }
                    >
                      <Delete />
                    </Avatar>
                  </Tooltip>
                </Grid>,
              ],
            }))}
          />
          <Confirmation
            open={this.state.confirm}
            title="Eliminar enfermedad"
            content="¿Está seguro que desea eliminar esta enfermedad?"
            id="audio"
            keepMounted
            handleOk={this.state.handleOk}
            handleCancel={() =>
              this.setState({ confirm: false, handleOk: () => {} })
            }
          />
        </div>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getDiseases}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withRouter(withAlert(Disease));
