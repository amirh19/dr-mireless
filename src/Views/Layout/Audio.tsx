import React from "react";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Add from "../../Components/ActionModals/Add";
import UserContext from "../../Context/User";
import AudioForm from "../../Components/Form/Audio";
import IAudio from "../../Classes/Interfaces/IAudio";
import SuperAdmin from "../../Classes/SuperAdmin";
import Staff from "../../Classes/Staff";
import Table from "../../Components/DataVisual/Table";
import Avatar from "@material-ui/core/Avatar";
import EditIcon from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import PlayIcon from "@material-ui/icons/PlayCircleOutline";
import StopIcon from "@material-ui/icons/Stop";
import Confirmation from "../../Components/ActionModals/Confirmation";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import withAlert from "../../Components/HOC/withAlert";
import Pagination from "../../Components/Buttons/Pagination";
import Tooltip from "@material-ui/core/Tooltip";

interface State extends IAudio {
  open: Boolean;
  edit: Boolean;
  audios: IAudio[];
  pagination: any;
  beingPlayed: HTMLAudioElement;
  beingPlayedIndex: Number;
  confirm: Boolean;
  handleOk: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}
class AudioLayout extends React.Component<
  { showAlert: (alert: AlertMessage) => void; showError: Function },
  State
> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = {
    id: 0,
    name: "",
    audio: null,
    order: 0,
    duration: 0,
    open: false,
    edit: false,
    audios: [],
    pagination: {
      first: 0,
      last: 0,
      next: 0,
      prev: 0,
    },
    beingPlayed: new Audio(),
    beingPlayedIndex: -1,
    confirm: false,
    handleOk: () => {},
  };

  async componentDidMount() {
    this.getAudios();
  }

  getAudios = async (page: Number = 1) => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.getAudios(page);
    if (response) {
      this.setState({
        audios: response.data,
        pagination: response.pagination,
      });
    } else {
      this.props.showError();
    }
  };

  handleModal = () => {
    this.setState((state) => ({
      open: !state.open,
      edit: false,
      id: 0,
      name: "",
      audio: null,
      order: 0,
      duration: 0,
    }));
  };

  addAudio = async () => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.addAudio({
      id: 0,
      name: this.state.name,
      audio: this.state.audio,
      order: this.state.order,
      duration: this.state.duration,
    });
    if (response) {
      const { audios } = this.state;
      audios.push(response as never);
      this.setState(
        {
          audios: audios,
          open: false,
        },
        () =>
          this.props.showAlert({
            message: "Se ha agregado una Biofrecuencia",
            severity: "success",
          })
      );
    } else {
      this.props.showError();
    }
  };
  updateAudio = async () => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.updateAudio({
      id: this.state.id,
      name: this.state.name,
      audio: this.state.audio,
      order: this.state.order,
      duration: this.state.duration,
    });
    if (response) {
      const { audios } = this.state;
      const updated = audios.map((audio: IAudio) => {
        if (audio.id === response.id) {
          return response;
        } else {
          return audio;
        }
      });
      this.setState({ audios: updated, open: false }, () =>
        this.props.showAlert({
          message: "Se ha actualizado una Biofrecuencia",
          severity: "success",
        })
      );
    } else {
      this.props.showError();
    }
  };

  deleteAudio = async (id: Number, index: Number) => {
    const worker = this.context.user as Staff | SuperAdmin;
    const response = await worker.deleteAudio(id);
    if (response) {
      const { audios } = this.state;
      audios.splice(index as number, 1);
      this.setState(
        { audios: audios, confirm: false, handleOk: () => {} },
        () =>
          this.props.showAlert({
            message: "Se ha eliminado una Biofrecuencia",
            severity: "success",
          })
      );
    } else {
      this.props.showError();
    }
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const key = event.currentTarget.name;
    const currentTarget = event.currentTarget;
    if (Object.keys(this.state).includes(key)) {
      if (key === "audio") {
        this.props.showAlert({
          message: "Se está subiendo un archivo",
          severity: "info",
        });
        const audio = new Audio(URL.createObjectURL(currentTarget.files![0]));
        const finish = () =>
          this.setState(({
            [key]: currentTarget.files![0],
            duration: audio.duration,
          } as unknown) as Pick<State, keyof State>);
        audio.onloadedmetadata = finish;
      } else {
        this.setState(({ [key]: event.currentTarget.value } as unknown) as Pick<
          State,
          keyof State
        >);
      }
    }
  };
  handleAudios = async (index: Number, audio: HTMLAudioElement) => {
    const { beingPlayed, beingPlayedIndex } = this.state;
    if (index === beingPlayedIndex) {
      if (beingPlayed.paused) {
        await beingPlayed.play();
      } else {
        beingPlayed.pause();
        beingPlayed.currentTime = 0;
      }
      this.setState({
        beingPlayed: beingPlayed,
        beingPlayedIndex: index,
      });
    } else {
      if (!beingPlayed.paused) {
        beingPlayed.pause();
        beingPlayed.currentTime = 0;
      }
      await audio.play();
      this.setState({
        beingPlayed: audio,
        beingPlayedIndex: index,
      });
    }
  };
  render() {
    return (
      <>
        <div>
          <Add open={this.state.open} handleModal={this.handleModal}>
            <>
              <AudioForm {...this.state} handleChange={this.handleChange} />
              <ButtonGroup
                style={{
                  marginTop: "2rem",
                  width: "100%",
                  display: "flex",
                  justifyContent: "center",
                  position: "absolute",
                  bottom: 10,
                }}
                size="large"
                color="default"
              >
                <Button
                  onClick={this.state.edit ? this.updateAudio : this.addAudio}
                >
                  {this.state.edit ? "Actualizar" : "Agregar"}
                </Button>
                <Button
                  onClick={() => this.setState({ open: false, edit: false })}
                >
                  Cancelar
                </Button>
              </ButtonGroup>
            </>
          </Add>
          <Table
            model="ID"
            attributes={["Biofrecuencia", "Duración", "Acciones"]}
            data={this.state.audios.map((audio: IAudio, index: Number): {
              name: String;
              values: any;
            } => ({
              name: audio.id.toString(),
              values: [
                `${audio.name}`,
                audio.duration,
                <Grid container direction="row" justify="flex-start">
                  <Tooltip
                    title={
                      this.state.beingPlayedIndex === index &&
                      !this.state.beingPlayed.paused
                        ? "Detener"
                        : "Reproducir"
                    }
                  >
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--secondary)",
                      }}
                      onClick={async () => {
                        await this.handleAudios(
                          index,
                          new Audio(audio.audio as string)
                        );
                      }}
                    >
                      {this.state.beingPlayedIndex === index &&
                      !this.state.beingPlayed.paused ? (
                        <StopIcon />
                      ) : (
                        <PlayIcon />
                      )}
                    </Avatar>
                  </Tooltip>
                  <Tooltip title="Editar">
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--secondary)",
                      }}
                      onClick={() => {
                        this.setState({
                          ...audio,
                          open: true,
                          edit: true,
                        });
                      }}
                    >
                      <EditIcon />
                    </Avatar>
                  </Tooltip>
                  <Tooltip title="Eliminar">
                    <Avatar
                      style={{
                        cursor: "pointer",
                        backgroundColor: "var(--danger)",
                      }}
                      onClick={() =>
                        this.setState({
                          confirm: true,
                          handleOk: () => this.deleteAudio(audio.id, index),
                        })
                      }
                    >
                      <Delete />
                    </Avatar>
                  </Tooltip>
                </Grid>,
              ],
            }))}
          />
          <Confirmation
            open={this.state.confirm}
            title="Eliminar biofrecuencia"
            content="¿Está seguro que desea eliminar esta biofrecuencia?"
            id="audio"
            keepMounted
            handleOk={this.state.handleOk}
            handleCancel={() =>
              this.setState({ confirm: false, handleOk: () => {} })
            }
          />
        </div>
        <Grid container justify="center">
          <Grid container justify="space-between" item sm={4} xs={8}>
            <Pagination
              pagination={this.state.pagination}
              getData={this.getAudios}
            />
          </Grid>
        </Grid>
      </>
    );
  }
}
export default withAlert(AudioLayout);
