import React from "react";
import UserContext from "../../Context/User";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { AlertMessage } from "../../Components/ActionModals/Toast";
import withAlert from "../../Components/HOC/withAlert";
import ITerapist from "../../Classes/Interfaces/ITerapist";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import IAppointment from "../../Classes/Interfaces/IAppointment";
import Calendar from "../../Components/Calendar/Calendar";
import { View, EventApi } from "@fullcalendar/core";
import Terapist from "../../Classes/Terapist";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Grid,
  Typography,
} from "@material-ui/core";

interface State extends IAppointment {
  open: boolean;
  edit: boolean;
  customerName: String;
  pagination: any;
  terapists: ITerapist[];
  events: IAppointment[];
  view: View | null;
}
interface Props extends RouteComponentProps<{ id?: string }> {
  showAlert: (alert: AlertMessage) => void;
  showError: Function;
}
const today = new Date();
today.setMinutes(0, 0, 0);
const initialState: State = {
  id: 0,
  date: today.toISOString().substr(0, 10),
  hours: today.toISOString().substr(11, 5),
  patient: 0, //customer id
  customerName: "",
  therapist: -1,
  status: 0,
  branchOffice: 0,
  open: false,
  edit: false,
  terapists: [],
  events: [],
  pagination: {
    first: 0,
    last: 0,
    next: 0,
    prev: 0,
  },
  view: null,
};

class MyAppointments extends React.Component<Props, State> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;
  state = initialState;

  handleModal = () => {
    this.setState((state) => ({
      ...initialState,
      open: !state.open,
      terapists: state.terapists,
      pagination: state.pagination,
    }));
  };

  onViewChange = (viewInfo: { el: HTMLElement; view: View }) => {
    this.getEvents(viewInfo.view);
  };

  getEvents = async (view: View) => {
    const worker = this.context.user as Terapist;
    const response = await worker.getAppointments(view);
    if (response) {
      this.setState({ view: view, events: response as IAppointment[] });
    } else {
      this.props.showError();
    }
  };

  getAppointment = async (id: String) => {
    const worker = this.context.user as Terapist;
    const response = await worker.getAppointment(id);
    let customer: String = "";
    if (response) {
      const custResponse = await worker.getCustomer(response.patient);
      if (custResponse) {
        customer =
          (custResponse as ICustomer).first_name +
          " " +
          (custResponse as ICustomer).last_name;
      }
      this.setState({
        ...response,
        customerName: customer,
        open: true,
        edit: true,
      });
    }
  };

  getStatus = (status: Number): String => {
    let statusName: String;
    switch (status) {
      case 1:
        statusName = "Cancelada";
        break;
      case 2:
        statusName = "Pendiente";
        break;
      case 3:
        statusName = "Confirmado";
        break;
      case 4:
        statusName = "Atendido";
        break;
      case 5:
        statusName = "No atendido";
        break;
      default:
        statusName = "No se encuentra la información disponible";
    }
    return statusName;
  };

  render() {
    return (
      <>
        <div>
          <Calendar
            onViewChange={this.onViewChange}
            events={this.state.events}
            handleDateClick={() => {}}
            eventClick={(args: {
              el: HTMLElement;
              event: EventApi;
              jsEvent: MouseEvent;
              view: View;
            }) => {
              this.getAppointment(args.event.id);
            }}
          />
        </div>
        <Dialog
          disableBackdropClick
          disableEscapeKeyDown
          maxWidth="xs"
          aria-labelledby="confirmation-dialog-title"
          open={this.state.open}
          style={{
            width: "100%",
            maxHeight: 435,
          }}
        >
          <DialogTitle id="confirmation-dialog-title">Cita</DialogTitle>
          <DialogContent dividers>
            <Grid container direction="row">
              <Typography variant="h6">
                Paciente: {this.state.customerName}
              </Typography>
            </Grid>
            <Grid container direction="row">
              <Typography variant="h6">
                Fecha:{" "}
                <Typography
                  component="span"
                  style={{ textTransform: "capitalize" }}
                  variant="h6"
                >
                  {new Date(
                    `${this.state.date}T${this.state.hours}:00.000Z`
                  ).toLocaleDateString("es-MX", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                  })}
                </Typography>
              </Typography>
            </Grid>
            <Grid container direction="row">
              <Typography variant="h6">Hora: {this.state.hours}</Typography>
            </Grid>
            <Grid container direction="row">
              <Typography variant="h6">
                Estatus: {this.getStatus(this.state.status)}
              </Typography>
            </Grid>
          </DialogContent>
          <DialogActions>
            {this.state.status === 3 && (
              <Button
                onClick={() =>
                  this.props.history.push(`consulta/${this.state.patient}`)
                }
                style={{ backgroundColor: "var(--principal)", color: "white" }}
              >
                Ir a Consulta
              </Button>
            )}
            <Button
              onClick={() =>
                this.setState((state) => ({
                  ...initialState,
                  therapist: state.therapist,
                  events: state.events,
                  open: false,
                }))
              }
              style={{ color: "var(--principal)" }}
            >
              Cerrar
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
}
export default withRouter(withAlert(MyAppointments) as any);
