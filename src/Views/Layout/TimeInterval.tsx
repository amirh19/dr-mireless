import React from "react";
import { Grid, TextField, Button } from "@material-ui/core";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import { TimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import DateFnsUtils from "@date-io/date-fns";
import { es } from "date-fns/locale";
import UserContext from "../../Context/User";
import SuperAdmin from "../../Classes/SuperAdmin";
import ITimeControl from "../../Classes/Interfaces/ITimeControl";
import moment from "moment";

interface Props extends withAlertProps {}
interface State extends ITimeControl {}
class TimeInterval extends React.Component<Props, State> {
  static contextType = UserContext;
  context!: React.ContextType<typeof UserContext>;

  state = {
    id: 1,
    initial_timetable: "00:00:00",
    final_timetable: "00:00:00",
    validity: 0,
    term: 1,
  };

  componentDidMount() {
    this.getTimeControl();
  }

  getTimeControl = async () => {
    const worker = this.context.user as SuperAdmin;
    const response = await worker.getTimeControl();
    if (response) {
      this.setState(
        {
          ...(response as ITimeControl),
        },
        () =>
          this.props.showAlert({
            message: "Se ha recuperado la configuración actual",
            severity: "info",
          })
      );
    } else {
      this.props.showError();
    }
  };

  save = async () => {
    const worker = this.context.user as SuperAdmin;
    const response = await worker.saveTimeControl(this.state);
    return response
      ? this.setState({ ...(response as ITimeControl) }, () => {
          this.props.showAlert({
            message: "Se ha guardado",
            severity: "success",
          });
        })
      : this.props.showError();
  };

  render() {
    const start = this.state.initial_timetable.split(":");
    const end = this.state.final_timetable.split(":");
    let startDate = new Date();
    let endDate = new Date();
    startDate.setHours(parseInt(start[0]), parseInt(start[1]), 0);
    endDate.setHours(parseInt(end[0]), parseInt(end[1]), 0);
    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={es}>
        <Grid container spacing={2}>
          <Grid item xs={12} container justify="center">
            <TimePicker
              ampm
              label="Inicio"
              value={startDate}
              onChange={(date: MaterialUiPickersDate) => {
                this.setState({
                  initial_timetable: moment(date).format("HH:mm:ss"),
                });
              }}
            />
          </Grid>
          <Grid item xs={12} container justify="center">
            <TimePicker
              ampm
              label="Fin"
              value={endDate}
              onChange={(date: MaterialUiPickersDate) => {
                this.setState({
                  final_timetable: moment(date).format("HH:mm:ss"),
                });
              }}
            />
          </Grid>
          <Grid item xs={12} container justify="center">
            <TextField
              type="number"
              label="Tiempo entre audios"
              value={this.state.term}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                this.setState({ term: parseInt(e.target.value) });
              }}
            />
          </Grid>
          <Grid item xs={12} container justify="center">
            <TextField
              type="number"
              label="Días de validez"
              value={this.state.validity}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                this.setState({ validity: parseInt(e.target.value) });
              }}
            />
          </Grid>
          <Grid item xs={12} container justify="center">
            <Button variant="contained" onClick={this.save} color="primary">
              Guardar
            </Button>
          </Grid>
        </Grid>
      </MuiPickersUtilsProvider>
    );
  }
}

export default withAlert(TimeInterval);
