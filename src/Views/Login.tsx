import React, { Component } from "react";
import User from "../Classes/User";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { Button, Container, Grid } from "@material-ui/core";
import ILoginData from "../Classes/Interfaces/ILoginData";
import { withRouter, RouteComponentProps } from "react-router-dom";
import UserContext from "../Context/User";
import Staff from "../Classes/Staff";
import SuperAdmin from "../Classes/SuperAdmin";
import Logo from "../assets/images/logotipo.png";
import Terapist from "../Classes/Terapist";
import Secretary from "../Classes/Secretary";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import Service from "../Classes/Service";
const inputStyle: React.CSSProperties = {
  width: "100%",
  backgroundColor: "var(--principal)",
};
const buttonStyle: React.CSSProperties = {
  color: "black",
  backgroundColor: "white",
  width: "100%",
};
const logo: React.CSSProperties = {
  height: "auto",
  width: "100%",
  backgroundColor: "var(--principal)",
};
const form: React.CSSProperties = {
  height: "100vh",
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "column",
  backgroundColor: "var(--principal)",
};
class Login extends Component<
  RouteComponentProps,
  ILoginData & { type: "password" | "text" }
> {
  static contextType = UserContext;
  constructor(props: RouteComponentProps) {
    super(props);
    this.state = {
      email: "",
      password: "",
      type: "password",
    };
  }
  private form = React.createRef<HTMLFormElement>();
  private handleInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const key = event.currentTarget.name;
    if (Object.keys(this.state).includes(key)) {
      this.setState(({ [key]: event.currentTarget.value } as unknown) as Pick<
        ILoginData,
        keyof ILoginData
      >);
    }
  };

  private handleSubmit = async (event: React.SyntheticEvent) => {
    try {
      event.preventDefault();
      const user = await User.login(this.state);
      await this._instanceUser(user);
    } catch (error) {}
  };

  private _instanceUser = async (user: any) => {
    if (user) {
      let currentUser: any;
      switch (user.type) {
        case 1: //SuperAdmin
          currentUser = new SuperAdmin(user);
          break;
        case 2: //Staff
          currentUser = new Staff(user);
          break;
        case 3: //Customer
          throw new Error("Acceso no autorizado");
        case 4:
          currentUser = new Terapist(user); //Terapist
          break;
        case 5:
          currentUser = new Secretary(user); //Asistant
          break;
        default:
      }
      if (!(currentUser instanceof SuperAdmin)) {
        await currentUser.setLocation();
      }
      this.context.setUser(currentUser);
      this.props.history.push("/");
    }
  };

  async componentDidMount() {
    try {
      const service: Service = new Service({});
      const user = await service.tokenAuth();
      if (user) {
        await this._instanceUser(user);
      }
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    return (
      <form ref={this.form} style={form}>
        <Container maxWidth="xs">
          <Grid container spacing={2}>
            <img alt="Doctor Mireles" style={logo} src={Logo} />
            <Grid item xs={12}>
              <TextField
                style={inputStyle}
                label="Usuario"
                name="email"
                type="email"
                required
                onChange={this.handleInput}
                InputLabelProps={{
                  style: {
                    color: "white",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                style={inputStyle}
                label="Contraseña"
                required
                name="password"
                type={this.state.type}
                onChange={this.handleInput}
                InputProps={{
                  endAdornment:
                    this.state.type === "password" ? (
                      <Visibility
                        style={{ cursor: "pointer" }}
                        onClick={() => this.setState({ type: "text" })}
                      />
                    ) : (
                      <VisibilityOff
                        style={{ cursor: "pointer" }}
                        onClick={() => this.setState({ type: "password" })}
                      />
                    ),
                }}
                InputLabelProps={{
                  style: {
                    color: "white",
                  },
                }}
              />
            </Grid>
            <Grid item xs={12}>
              <Button style={buttonStyle} onClick={this.handleSubmit}>
                <Typography variant="body1">Enviar</Typography>
              </Button>
            </Grid>
          </Grid>
        </Container>
      </form>
    );
  }
}
export default withRouter(Login as any) as React.ComponentType;
