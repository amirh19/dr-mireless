import * as React from "react";
import FullCalendar from "@fullcalendar/react";
import { EventApi, View } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import esLocale from "@fullcalendar/core/locales/es";
import "./main.css";
import IAppointment from "../../Classes/Interfaces/IAppointment";
import moment from "moment";

interface State {
  calendarWeekends: boolean;
}
interface Props {
  events: IAppointment[];
  handleDateClick: (arg: {
    date: Date;
    dateStr: string;
    allDay: boolean;
    resource?: any;
    dayEl: HTMLElement;
    jsEvent: MouseEvent;
    view: View;
  }) => void;
  eventClick: (arg: {
    el: HTMLElement;
    event: EventApi;
    jsEvent: MouseEvent;
    view: View;
  }) => boolean | void;
  onViewChange: (viewInfo: { el: HTMLElement; view: View }) => void;
}
export default class Calendar extends React.Component<Props, State> {
  calendarComponentRef = React.createRef<FullCalendar>();

  constructor(props: Props) {
    super(props);

    this.state = {
      calendarWeekends: true,
      //[
      // initial event data
      // { id: 123, title: "Event Now", start: new Date() },
      //],
    };
  }

  handleDateClick = (arg: {
    date: Date;
    dateStr: string;
    allDay: boolean;
    resource?: any;
    dayEl: HTMLElement;
    jsEvent: MouseEvent;
    view: View;
  }) => {
    this.calendarComponentRef
      .current!.getApi()
      .changeView("timeGridDay", arg.date.toISOString().substr(0, 10));
  };

  render() {
    return (
      <div className="demo-app">
        <div className="demo-app-top"></div>
        <div className="demo-app-calendar">
          <FullCalendar
            defaultView="dayGridMonth"
            header={{
              left: "prev,next today",
              center: "title",
              right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
            }}
            slotLabelFormat={{
              hour12: false,
              hour: "2-digit",
              minute: "2-digit",
            }}
            eventTimeFormat={{
              hour12: false,
              hour: "2-digit",
              minute: "2-digit",
            }}
            plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
            ref={this.calendarComponentRef}
            weekends={this.state.calendarWeekends}
            events={this.props.events.map((event: IAppointment) => {
              const start = moment(`${event.date} ${event.hours}:00`);
              const end = moment(start).add("15", "minutes");
              return {
                id: event.id ? event.id.toString() : "0",
                title: "",
                start: start.toDate(),
                end: end.toDate(),
              };
            })}
            dateClick={this.handleDateClick}
            locales={[esLocale as any]}
            locale={"es"}
            slotDuration={"00:15:00"}
            minTime={"08:00:00"}
            maxTime={"20:30:00"}
            eventClick={this.props.eventClick}
            datesRender={this.props.onViewChange}
            hiddenDays={[0]}
          />
        </div>
      </div>
    );
  }
}
