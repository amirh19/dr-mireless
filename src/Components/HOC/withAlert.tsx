import React from "react";
import ToastContext from "../../Context/Toast";
import { AlertMessage } from "../ActionModals/Toast";
export interface withAlertProps {
  showAlert: (alert: AlertMessage) => void;
  showError: Function;
}
const withAlert = <P extends withAlertProps>(
  Component: React.ComponentType<P>
): React.ComponentType<Pick<P, Exclude<keyof P, keyof withAlertProps>>> => {
  return class extends React.Component<
    Pick<P, Exclude<keyof P, keyof withAlertProps>>
  > {
    static displayName = "WithAlert";
    static contextType = ToastContext;
    context!: React.ContextType<typeof ToastContext>;
    render() {
      return (
        <Component
          {...(this.props as P)}
          showAlert={(alert: AlertMessage) => {
            this.context.setAlert(alert);
          }}
          showError={() => {
            const error = localStorage.getItem("error");
            this.context.setAlert({
              message: error || "",
              severity: "error",
            });
            localStorage.setItem("error", "");
          }}
        />
      );
    }
  };
};
export default withAlert;
