import React from "react";
import {
  Avatar,
  Tooltip,
  Typography,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Grid,
  CircularProgress,
} from "@material-ui/core";
import SaveAltIcon from "@material-ui/icons/SaveAlt";
import XLSX from "xlsx";
import moment from "moment";
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import { es } from "date-fns/locale";
import Service from "../../Classes/Service";

interface Props {
  title: String;
  col_titles: Array<String>;
  endpoint: String;
  withDateRange: Boolean;
  property?: String;
  onStartChange?: (date: MaterialUiPickersDate) => void;
  onEndChange?: (date: MaterialUiPickersDate) => void;
  extraProcess?: Function;
  start?: Date;
  end?: Date;
}
interface State {
  start: Date;
  end: Date;
  load: Boolean;
}
class ExportXSLX extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    const startOfMonth = moment().startOf("month").toDate();
    this.state = {
      start: this.props.start || startOfMonth,
      end: this.props.end || moment().toDate(),
      load: false,
    };
  }
  export = async () => {
    this.setState({ load: true });
    let returnedData;
    const finalEndpoint =
      this.props.endpoint +
      (this.props.withDateRange
        ? `&start_range=${moment(this.state.start).format(
            "YYYY-MM-DD"
          )}&end_range=${moment(this.state.end).format("YYYY-MM-DD")}`
        : "");
    const service: Service = new Service({});
    const touched = await service.get(finalEndpoint);
    if (!touched.error) {
      returnedData = await service.get(
        finalEndpoint + "&per_page=" + touched.pagination.total_rows
      );
      returnedData =
        returnedData.data instanceof Array
          ? returnedData.data
          : returnedData.data[this.props.property as string];
    } else {
      returnedData = [];
    }
    if (typeof this.props.extraProcess === "function") {
      returnedData = await this.props.extraProcess(returnedData);
    }
    const wb = XLSX.utils.book_new();
    wb.Props = {
      Title: this.props.title + moment().format("DD/MM/YYYY"),
      CreatedDate: new Date(),
      Author: "DR MIRELES",
    };
    wb.SheetNames.push("Libro 1");
    let data: Array<Array<String>> = [];
    data.push(this.props.col_titles);
    returnedData.forEach((item: any) => {
      let array: Array<any> = [];
      for (let key in item) {
        array.push(item[key]);
      }
      data.push(array);
    });
    let worksheet = XLSX.utils.aoa_to_sheet(data);
    wb.Sheets["Libro 1"] = worksheet;
    XLSX.writeFile(wb, this.props.title + ".xlsx");
    this.setState({ load: false });
  };

  s2ab = (s: string) => {
    const buf = new ArrayBuffer(s.length); //convert s to arrayBuffer
    const view = new Uint8Array(buf); //create uint8array as viewer
    for (let i = 0; i < s.length; i++) view[i] = s.charCodeAt(i) & 0xff; //convert to octet
    return buf;
  };

  render() {
    return (
      <>
        {this.props.withDateRange && (
          <>
            <ExpansionPanel>
              <ExpansionPanelSummary>
                <Typography
                  style={{
                    width: "100%",
                    textAlign: "center",
                    fontWeight: "bold",
                    color: "var(--principal)",
                  }}
                  variant="body1"
                >
                  Filtrar por fecha
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={es}>
                  <Grid container>
                    <Grid item xs={12}>
                      <DatePicker
                        format="dd-MM-yyyy"
                        fullWidth
                        label="Desde:"
                        value={this.state.start}
                        onChange={(date: MaterialUiPickersDate) => {
                          this.setState({ start: date as Date });
                          if (typeof this.props.onStartChange === "function") {
                            this.props.onStartChange(date);
                          }
                        }}
                        maxDate={this.state.end}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <DatePicker
                        format="dd-MM-yyyy"
                        fullWidth
                        label="Hasta:"
                        value={this.state.end}
                        onChange={(date: MaterialUiPickersDate) => {
                          this.setState({ end: date as Date });
                          if (typeof this.props.onEndChange === "function") {
                            this.props.onEndChange(date);
                          }
                        }}
                        minDate={this.state.start}
                      />
                    </Grid>
                  </Grid>
                </MuiPickersUtilsProvider>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </>
        )}
        <Tooltip title="Exportar todo aplicando filtros">
          {this.state.load ? (
            <CircularProgress
              style={{
                fontSize: "0.875rem",
                height: "0.875rem",
                width: "0.875rem",
                color: "var(--principal)",
              }}
              variant="indeterminate"
            />
          ) : (
            <Avatar
              onClick={this.export}
              style={{
                margin: "1rem",
                cursor: "pointer",
                backgroundColor: "var(--principal)",
              }}
            >
              <SaveAltIcon style={{ color: "white" }} />
            </Avatar>
          )}
        </Tooltip>
      </>
    );
  }
}
export default ExportXSLX;
