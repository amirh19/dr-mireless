import React, { ReactElement } from "react";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Avatar, Modal, Tooltip } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    add: {
      backgroundColor: "var(--principal)",
      height: 36,
      width: 36,
      marginBottom: "2rem",
      cursor: "pointer",
    },
    addSecondary: {
      cursor: "pointer",
      backgroundColor: "var(--secondary)",
    },
    modal: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    modalContent: {
      position: "relative",
      backgroundColor: "var(--complement)",
      minHeight: "60vh",
      minWidth: "60vw",
      padding: 10,
    },
  })
);

interface Props {
  handleModal: Function;
  open: boolean;
  children: ReactElement;
  icon?: ReactElement;
  lightBlue?: Boolean;
  tooltip?: String;
}

const Add = (props: Props) => {
  const classes = useStyles();
  return (
    <>
      <Tooltip title={props.tooltip || "Agregar"}>
        <Avatar
          className={props.lightBlue ? classes.addSecondary : classes.add}
          onClick={() => props.handleModal()}
        >
          {props.icon ? props.icon : <AddIcon />}
        </Avatar>
      </Tooltip>
      <Modal
        open={props.open}
        onClose={() => props.handleModal()}
        className={classes.modal}
      >
        <div className={classes.modalContent}>{props.children}</div>
      </Modal>
    </>
  );
};
export default Add;
