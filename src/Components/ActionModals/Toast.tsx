import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import { makeStyles, Theme } from "@material-ui/core/styles";

export interface AlertMessage {
  message: String;
  severity: "error" | "warning" | "info" | "success";
}

const Alert = (props: AlertProps) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));
interface ToastProps {
  alerts: AlertMessage[];
  handleClose: (index: number) => void;
}
const Toast = (props: ToastProps) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {props.alerts.map((alert: AlertMessage, index: number) => (
        <Snackbar
          key={index}
          style={{ bottom: `calc(24px*${index + 1} + 48px*${index})` }}
          open={true}
        >
          <Alert
            onClose={() => props.handleClose(index)}
            severity={alert.severity}
          >
            {alert.message}
          </Alert>
        </Snackbar>
      ))}
    </div>
  );
};
export default Toast;
