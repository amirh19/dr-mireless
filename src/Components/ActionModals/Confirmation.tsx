import React from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import Button from "@material-ui/core/Button";

interface ConfirmationDialogRawProps {
  id: string;
  keepMounted: boolean;
  open: boolean;
  title: String;
  content: String;
  handleOk: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  handleCancel: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;
}
export default function ConfirmationDialogRaw(
  props: ConfirmationDialogRawProps
) {
  const { handleOk, handleCancel, content, open, title, ...other } = props;

  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      aria-labelledby="confirmation-dialog-title"
      open={open}
      {...other}
      style={{
        width: "80%",
        maxHeight: 435,
      }}
    >
      <DialogTitle id="confirmation-dialog-title">{title}</DialogTitle>
      <DialogContent dividers>{content}</DialogContent>
      <DialogActions>
        <Button autoFocus onClick={handleCancel} color="primary">
          Cancelar
        </Button>
        <Button onClick={handleOk} color="primary">
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}
