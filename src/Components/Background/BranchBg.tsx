import React from "react";
import Logo from "../../assets/images/logotipo.png";
import CircularProgress from "@material-ui/core/CircularProgress";

interface Props {
  withActivity?: Boolean;
}
const BranchBg: React.FC<Props> = (props: Props) => (
  <div
    style={{
      minHeight: "100%",
      minWidth: "100%",
      top: 0,
      left: 0,
      position: "absolute",
      backgroundColor: "var(--principal)",
    }}
  >
    <img
      alt="dr mireles"
      src={Logo}
      style={{
        position: "absolute",
        top: "50%",
        left: "50%",
        backgroundColor: "var(--principal)",
        height: "10rem",
        width: "auto",
        transform: "translate(-50%, -50%)",
      }}
    />
    {props.withActivity && (
      <CircularProgress
        style={{
          color: "white",
          position: "absolute",
          top: "55%",
          left: "50%",
        }}
      />
    )}
  </div>
);
export default BranchBg;
