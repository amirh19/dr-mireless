import React, { ChangeEventHandler } from "react";
import { Container, Grid, TextField, Typography } from "@material-ui/core";
import IDisease from "../../Classes/Interfaces/IDisease";
import IAudio from "../../Classes/Interfaces/IAudio";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import { Container as DragZone } from "../DropZone/DropZone";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import AddIcon from "@material-ui/icons/Add";
import Avatar from "@material-ui/core/Avatar";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import Previous from "@material-ui/icons/ChevronLeft";
import Next from "@material-ui/icons/ChevronRight";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    input: {
      width: "100%",
    },
  })
);
interface Props extends IDisease {
  handleChange: ChangeEventHandler;
  allAudios: IAudio[];
  audios: IAudio[];
  audioPagination: any;
  addAudio: Function;
  deleteAudio: Function;
  getOrderedAudios: Function;
  getPage: Function;
}
const DiseaseForm = (props: Props) => {
  const classes = useStyles();
  return (
    <form>
      <Container maxWidth="md">
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              className={classes.input}
              label="Nombre"
              value={props.name}
              name="name"
              onChange={props.handleChange}
            />
          </Grid>
          <Grid
            style={{
              border: "1px solid rgba(0, 0, 0, 0.13)",
              boxShadow: "#00000024 0 0 4px 4px",
              borderRadius: 5,
              height: 200,
              overflowY: "auto",
            }}
            item
            xs={6}
          >
            <Typography variant="body1">Todos los audios</Typography>
            {props.allAudios.map((audio: IAudio) => {
              return props.audios.findIndex(
                (item: IAudio) => item.id === audio.id
              ) !== -1 ? null : (
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    border: "1px solid rgba(0, 0, 0, 0.14)",
                    borderRadius: 20,
                    height: 35,
                    alignItems: "center",
                    marginBottom: 10,
                  }}
                >
                  <Typography
                    style={{ marginRight: 10, marginLeft: 10 }}
                    variant="body1"
                  >
                    {audio.name}
                  </Typography>
                  <Avatar
                    style={{
                      marginRight: 10,
                      marginLeft: 10,
                      height: "1.2rem",
                      width: "1.2rem",
                      backgroundColor: "var(--secondary)",
                    }}
                    onClick={() => {
                      props.addAudio(audio);
                    }}
                  >
                    <AddIcon style={{ color: "white" }} fontSize="small" />
                  </Avatar>
                </div>
              );
            })}
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-around",
              }}
            >
              {props.audioPagination.first && (
                <FirstPageIcon
                  style={{ cursor: "pointer" }}
                  onClick={() => props.getPage(props.audioPagination.first)}
                />
              )}
              {props.audioPagination.prev && (
                <Previous
                  style={{ cursor: "pointer" }}
                  onClick={() => props.getPage(props.audioPagination.prev)}
                />
              )}
              {props.audioPagination.next && (
                <Next
                  style={{ cursor: "pointer" }}
                  onClick={() => props.getPage(props.audioPagination.next)}
                />
              )}
              {props.audioPagination.last && (
                <LastPageIcon
                  style={{ cursor: "pointer" }}
                  onClick={() => props.getPage(props.audioPagination.last)}
                />
              )}
            </div>
          </Grid>
          <Grid
            style={{
              border: "1px solid rgba(0, 0, 0, 0.13)",
              boxShadow: "#00000024 0 0 4px 4px",
              borderRadius: 5,
              height: 200,
              overflowY: "auto",
            }}
            item
            xs={6}
          >
            <Typography variant="body1">Seleccionados</Typography>
            <DndProvider backend={HTML5Backend}>
              <DragZone
                data={props.audios}
                setCards={props.getOrderedAudios}
                deleteAudio={props.deleteAudio}
              />
            </DndProvider>
          </Grid>
        </Grid>
      </Container>
    </form>
  );
};

export default DiseaseForm;
