import React from "react";
import { Chip, Grid, Typography } from "@material-ui/core";
import UserContext from "../../Context/User";
import Secretary from "../../Classes/Secretary";
import SuperAdmin from "../../Classes/SuperAdmin";
import ITreatment from "../../Classes/Interfaces/ITreatment";
import IAudio from "../../Classes/Interfaces/IAudio";
import withAlert, { withAlertProps } from "../../Components/HOC/withAlert";
import Pagination from "../../Components/Buttons/Pagination";

interface IPagination {
  first: Number;
  next: Number;
  last: Number;
  prev: Number;
}

interface Props extends withAlertProps {
  patient: Number;
}

const TreatmentsHistory: React.FC<Props> = (props: Props) => {
  const [treatments, setTreatments] = React.useState<ITreatment[]>([]);
  const [pagination, setPagination] = React.useState<IPagination>({
    first: 0,
    prev: 0,
    next: 0,
    last: 0,
  });
  const context = React.useContext<UserContext>(UserContext);

  const getTreatments = async (page: Number = 1) => {
    const worker = context.user as Secretary | SuperAdmin;
    const response = await worker.getTreatments(props.patient, page);
    if (response) {
      setTreatments(response.data);
      setPagination(response.pagination);
    } else {
      props.showError();
    }
  };

  React.useEffect(() => {
    getTreatments();
  }, [props.patient]);

  return (
    <>
      <Grid container style={{ height: "60vh", overflowY: "auto" }}>
        {treatments.map((treatment: ITreatment) => (
          <Grid item xs={6}>
            <Grid container>
              <Grid item xs={12}>
                <Typography variant="body1">
                  Enfermedad: {treatment.name}
                </Typography>
                <Typography variant="body1">
                  Días de tratamiento: {treatment.days_of_treatment}
                </Typography>
                <Typography variant="body1">
                  Fecha de creación:{" "}
                  {new Date(treatment.date as string).toLocaleDateString(
                    "es-MX",
                    {
                      year: "numeric",
                      month: "2-digit",
                      day: "2-digit",
                    }
                  )}
                </Typography>
              </Grid>
              <Grid item container direction="column" xs={12}>
                {(treatment.audio as IAudio[]).map((audio: IAudio) => (
                  <Chip style={{ marginBottom: 5 }} label={audio.name} />
                ))}
              </Grid>
            </Grid>
          </Grid>
        ))}
        {treatments.length === 0 && (
          <Grid item container justify="center" alignItems="center" xs={12}>
            <Typography variant="body1">
              Aún no cuenta con ningún tratamiento
            </Typography>
          </Grid>
        )}
      </Grid>
      <Grid item container justify="center" xs={12}>
        <Pagination pagination={pagination} getData={getTreatments} />
      </Grid>
    </>
  );
};

export default withAlert(TreatmentsHistory);
