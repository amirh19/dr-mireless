import React from "react";
import {
  Container,
  Grid,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  Radio,
  FormControlLabel,
  Typography,
  Divider,
} from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import ICustomer from "../../Classes/Interfaces/ICustomer";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import IBillingData from "../../Classes/Interfaces/IBillingData";
import DateFnsUtils from "@date-io/date-fns";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { MuiPickersOverrides } from "@material-ui/pickers/typings/overrides";
import moment from "moment";

const materialTheme = createMuiTheme({
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: "var(--principal)",
      },
    },
    MuiPickersDay: {
      day: {
        color: "var(--secondary)",
      },
      daySelected: {
        backgroundColor: "var(--principal)",
      },
      current: {
        color: "var(--principal)",
      },
    },
  },
});
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    input: {
      width: "100%",
    },
  })
);
interface Props
  extends Omit<ICustomer, "birthday">,
    Omit<IBillingData, "id" | "email"> {
  birthday: Date;
  password: String;
  password_confirm: String;
  billing_email: String;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleBirthdayChange: (date: MaterialUiPickersDate) => void;
  edit: Boolean;
}
const AddCustomer = (props: Props) => {
  function getAge(date: Date) {
    var today = new Date();
    var birthDate = date;
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }
  const classes = useStyles();
  return (
    <form
      style={{
        height: "60vh",
        marginTop: "2rem",
        marginBottom: "4rem",
        overflowY: "auto",
      }}
    >
      <Container maxWidth="md">
        <Grid container spacing={2}>
          <Grid container justify="center" item xs={12}>
            <Typography variant="body1">Datos generales</Typography>
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Nombre del paciente"
              value={props.first_name}
              onChange={props.handleChange}
              name="first_name"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Apellidos"
              value={props.last_name}
              onChange={props.handleChange}
              name="last_name"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Teléfono"
              value={props.phone}
              onChange={props.handleChange}
              name="phone"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Correo electrónico"
              value={props.email}
              onChange={props.handleChange}
              name="email"
              type="email"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <FormControl component="fieldset">
              <FormLabel component="legend">Sexo</FormLabel>
              <RadioGroup
                aria-label="gender"
                name="gender"
                value={props.gender}
                onChange={props.handleChange}
              >
                <FormControlLabel value={2} control={<Radio />} label="Mujer" />
                <FormControlLabel
                  value={1}
                  control={<Radio />}
                  label="Hombre"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item sm={6} xs={12}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <ThemeProvider theme={materialTheme}>
                <KeyboardDatePicker
                  disableToolbar
                  variant="inline"
                  format="dd-MM-yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  label="Fecha de nacimiento"
                  value={moment(props.birthday).toDate()}
                  onChange={props.handleBirthdayChange}
                  KeyboardButtonProps={{
                    "aria-label": "change date",
                  }}
                  openTo="year"
                  maxDate={new Date()}
                />
              </ThemeProvider>
            </MuiPickersUtilsProvider>
            <Typography variant="body1">
              Edad: {getAge(props.birthday)}
            </Typography>
          </Grid>

          {!props.edit ? (
            <>
              <Grid item sm={6} xs={12}>
                <TextField
                  className={classes.input}
                  label="Contraseña"
                  value={props.password}
                  onChange={props.handleChange}
                  name="password"
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  className={classes.input}
                  label="Confirmar contraseña"
                  value={props.password_confirm}
                  onChange={props.handleChange}
                  name="password_confirm"
                  error={props.password_confirm !== props.password}
                  helperText={
                    props.password_confirm !== props.password
                      ? "La confirmación debe coincidir con la contraseña"
                      : ""
                  }
                />
              </Grid>
            </>
          ) : null}
          <Divider
            style={{ marginTop: "2rem", marginBottom: "2rem", width: "100%" }}
          />
          <Grid container justify="center" item xs={12}>
            <Typography variant="body1">Datos de facturación</Typography>
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Razón social"
              value={props.BusinessName}
              onChange={props.handleChange}
              name="BusinessName"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="RFC"
              value={props.rfc}
              onChange={props.handleChange}
              name="rfc"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Correo electrónico"
              value={props.billing_email}
              onChange={props.handleChange}
              name="billing_email"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Dirección"
              value={props.address}
              onChange={props.handleChange}
              name="address"
            />
          </Grid>

          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Ciudad"
              value={props.city}
              onChange={props.handleChange}
              name="city"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Estado"
              value={props.state}
              onChange={props.handleChange}
              name="state"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Código postal"
              value={props.cp}
              onChange={props.handleChange}
              name="cp"
            />
          </Grid>
        </Grid>
      </Container>
    </form>
  );
};
export default AddCustomer;
type overridesNameToClassKey = {
  [P in keyof MuiPickersOverrides]: keyof MuiPickersOverrides[P];
};

declare module "@material-ui/core/styles/overrides" {
  export interface ComponentNameToClassKey extends overridesNameToClassKey {}
}
