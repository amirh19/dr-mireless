import React, { ChangeEvent } from "react";
import { TextField } from "@material-ui/core";
import ITerapist from "../../Classes/Interfaces/ITerapist";
import ILocation from "../../Classes/Interfaces/ILocation";
import {
  Grid,
  Container,
  FormControl,
  FormLabel,
  InputLabel,
  Select,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import DateFnsUtils from "@date-io/date-fns";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import { createMuiTheme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";
import { MuiPickersOverrides } from "@material-ui/pickers/typings/overrides";
import { es } from "date-fns/locale";
import moment from "moment";

const materialTheme = createMuiTheme({
  overrides: {
    MuiPickersToolbar: {
      toolbar: {
        backgroundColor: "var(--principal)",
      },
    },
    MuiPickersDay: {
      day: {
        color: "var(--secondary)",
      },
      daySelected: {
        backgroundColor: "var(--principal)",
      },
      current: {
        color: "var(--principal)",
      },
    },
  },
});
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    input: {
      width: "100%",
    },
  })
);
interface Props extends Omit<ITerapist, "birthday"> {
  birthday: Date;
  type: 2 | 4 | 5;
  password: String;
  password_confirm: String;
  locations: ILocation[];
  location: Number;
  handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
  handleLocationChange: (
    event: React.ChangeEvent<{ name?: string; value: unknown }>
  ) => void;
  handleBirthdayChange: (date: MaterialUiPickersDate) => void;
  edit: Boolean;
}
const AddUser = (props: Props) => {
  const classes = useStyles();
  return (
    <form style={{ height: "60vh", overflowY: "auto", marginBottom: "4rem" }}>
      <Container maxWidth="md">
        <Grid container spacing={2}>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Nombre"
              value={props.first_name}
              onChange={props.handleChange}
              name="first_name"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Apellidos"
              value={props.last_name}
              onChange={props.handleChange}
              name="last_name"
            />
          </Grid>
          {props.type === 4 && (
            <>
              <Grid item sm={6} xs={12}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Sexo</FormLabel>
                  <RadioGroup
                    aria-label="gender"
                    name="gender"
                    value={props.gender}
                    onChange={props.handleChange}
                  >
                    <FormControlLabel
                      value={2}
                      control={<Radio />}
                      label="Mujer"
                    />
                    <FormControlLabel
                      value={1}
                      control={<Radio />}
                      label="Hombre"
                    />
                  </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item sm={6} xs={12}>
                <MuiPickersUtilsProvider utils={DateFnsUtils} locale={es}>
                  <ThemeProvider theme={materialTheme}>
                    <KeyboardDatePicker
                      className={classes.input}
                      disableToolbar
                      variant="inline"
                      format="dd-MM-yyyy"
                      margin="normal"
                      id="date-picker-inline"
                      label="Fecha de nacimiento"
                      value={moment(props.birthday).toDate()}
                      onChange={props.handleBirthdayChange}
                      KeyboardButtonProps={{
                        "aria-label": "change date",
                      }}
                      openTo="year"
                      maxDate={new Date()}
                    />
                  </ThemeProvider>
                </MuiPickersUtilsProvider>
              </Grid>
            </>
          )}
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Celular"
              value={props.phone}
              onChange={props.handleChange}
              name="phone"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Email"
              value={props.email}
              onChange={props.handleChange}
              name="email"
              type="email"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Contraseña"
              value={props.password}
              onChange={props.handleChange}
              name="password"
              error={props.password.length < 8 && props.password !== ""}
              helperText={
                props.password.length < 8
                  ? "La contraseña debe ser mayor a 8 caracteres"
                  : ""
              }
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Confirmar contraseña"
              value={props.password_confirm}
              onChange={props.handleChange}
              name="password_confirm"
              error={props.password_confirm !== props.password}
              helperText={
                props.password_confirm !== props.password
                  ? "La confirmación debe coincidir con la contraseña"
                  : ""
              }
            />
          </Grid>
          {props.type === 4 && (
            <>
              <Grid item sm={6} xs={12}>
                <TextField
                  className={classes.input}
                  label="Cédula Profesional"
                  value={props.professional_license}
                  onChange={props.handleChange}
                  name="professional_license"
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  className={classes.input}
                  label="Especialidad"
                  value={props.specialty}
                  onChange={props.handleChange}
                  name="specialty"
                />
              </Grid>
              <Grid item sm={6} xs={12}>
                <TextField
                  className={classes.input}
                  label="Cédula de Especialidad"
                  value={props.specialty_license}
                  onChange={props.handleChange}
                  name="specialty_license"
                />
              </Grid>
            </>
          )}
          <Grid item sm={12}>
            <FormControl style={{ width: "100%" }}>
              <InputLabel htmlFor="type">Sucursal</InputLabel>
              <Select
                native
                value={props.location}
                onChange={props.handleLocationChange}
              >
                {props.locations.map((location: ILocation) => (
                  <option value={location.id as number}>{location.name}</option>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </Container>
    </form>
  );
};
export default AddUser;

type overridesNameToClassKey = {
  [P in keyof MuiPickersOverrides]: keyof MuiPickersOverrides[P];
};

declare module "@material-ui/core/styles/overrides" {
  export interface ComponentNameToClassKey extends overridesNameToClassKey {}
}
