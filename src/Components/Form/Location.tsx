import React, { ChangeEvent } from "react";
import { TextField } from "@material-ui/core";
import ILocation from "../../Classes/Interfaces/ILocation";
import { Grid, Container } from "@material-ui/core";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    input: {
      width: "100%",
    },
  })
);
interface Props extends ILocation {
  handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
}
const AddLocation = (props: Props) => {
  const classes = useStyles();
  return (
    <form>
      <Container maxWidth="md">
        <Grid container spacing={2}>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Nombre del consultorio"
              value={props.name}
              onChange={props.handleChange}
              name="name"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Nombre corto"
              value={props.shortName}
              onChange={props.handleChange}
              name="shortName"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Teléfono"
              value={props.phone_number}
              onChange={props.handleChange}
              name="phone_number"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Dirección"
              value={props.address}
              onChange={props.handleChange}
              name="address"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Ciudad"
              value={props.city}
              onChange={props.handleChange}
              name="city"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Estado"
              value={props.state}
              onChange={props.handleChange}
              name="state"
            />
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField
              className={classes.input}
              label="Código postal"
              value={props.postalCode}
              onChange={props.handleChange}
              name="postalCode"
            />
          </Grid>
        </Grid>
      </Container>
    </form>
  );
};
export default AddLocation;
