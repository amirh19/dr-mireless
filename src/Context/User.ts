import React from "react";
import SuperAdmin from "../Classes/SuperAdmin";
import Staff from "../Classes/Staff";
import Secretary from "../Classes/Secretary";
import Terapist from "../Classes/Terapist";
interface UserContext {
  user: SuperAdmin | Terapist | Secretary | Staff | String;
  setUser: Function;
}
const Context: UserContext = {
  user: "",
  setUser: () => {},
};
const UserContext = React.createContext<UserContext>(Context);
export default UserContext;
